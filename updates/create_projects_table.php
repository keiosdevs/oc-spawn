<?php namespace OctExchange\Spawn\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateProjectsTable
 * @package OctExchange\Spawn\Updates
 */
class CreateProjectsTable extends Migration
{
    /**
     *
     */
    public function up()
    {
        Schema::create(
            'octexchange_spawn_projects',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('project_key')->index();
                $table->text('plugins')->nullable();
                $table->timestamps();
            }
        );
    }

    /**
     *
     */
    public function down()
    {
        Schema::dropIfExists('octexchange_spawn_projects');
    }
}
