<?php namespace OctExchange\Spawn\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUpdateLogsTable extends Migration
{
    public function up()
    {
        Schema::create(
            'octexchange_spawn_update_logs',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('update_details');
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('octexchange_spawn_update_logs');
    }
}
