<?php namespace OctExchange\Spawn\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class AddThemesToProject extends Migration
{

    public function up()
    {
        Schema::table(
            'octexchange_spawn_projects',
            function (Blueprint $table) {
                $table->text('themes')->after('theme');
            }
        );
    }

    public function down()
    {
        Schema::table(
            'octexchange_spawn_projects',
            function (Blueprint $table) {
                $table->dropColumn('themes');
            }
        );
    }

}
