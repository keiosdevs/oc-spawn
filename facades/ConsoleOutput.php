<?php

namespace OctExchange\Spawn\Facades;

use Illuminate\Support\Facades\Facade;

class ConsoleOutput extends Facade {

    protected static function getFacadeAccessor() {
        return 'consoleOutput';
    }

}