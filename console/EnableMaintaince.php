<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/28/17
 * Time: 4:42 PM
 */

namespace OctExchange\Spawn\Console;

use Cms\Models\MaintenanceSetting;
use Illuminate\Console\Command;
use OctExchange\Spawn\Facades\ConsoleOutput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class EnableMaintaince
 * @package OctExchange\Spawn\Console
 */
class EnableMaintaince extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'exchange:maint';
    /**
     * @var string The console command description.
     */
    protected $description = 'Sets october maintenance mode.';

    /**
     * @var
     */
    private $themeMap;

    public function handle()
    {
        $this->fire();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $settings = $this->getSettings();
        if (!$settings) {
            $this->info('No settings detected. Skipping maintenance.');

            return null;
        }
        $this->themeMap = json_encode($settings->theme_map);
        $inMaintenance = $settings->is_enabled;

        if ($inMaintenance) {
            $this->disableMaintenance();
            $this->info(\Lang::trans('octexchange.spawn::lang.strings.maintenance_disabled'));
        } else {
            $this->enableMaintenance();
            $this->warn(\Lang::trans('octexchange.spawn::lang.strings.maintenance_enabled'));
        }
        $this->warn('Clearing cache...');
        \Artisan::call('cache:clear');
        if (PHP_SAPI === 'cli') {
            ConsoleOutput::writeln('<info>'.\Artisan::output().'</info>');
        }

    }

    /**
     *
     */
    private function enableMaintenance()
    {
        $array = [
            'is_enabled' => true,
            'theme_map'  => json_decode($this->themeMap, true),
        ];
        $json = json_encode($array);
        \DB::table('system_settings')->where('item', 'cms_maintenance_settings')->update(
            [
                'value' => $json,
            ]
        );
    }

    /**
     *
     */
    private function disableMaintenance()
    {
        $array = [
            'is_enabled' => false,
            'theme_map'  => json_decode($this->themeMap, true),
        ];
        $json = json_encode($array);
        \DB::table('system_settings')->where('item', 'cms_maintenance_settings')->update(
            [
                'value' => $json,
            ]
        );
    }

    /**
     * @return mixed|null
     */
    private function getSettings()
    {
        $record = \DB::table('system_settings')->where('item', 'cms_maintenance_settings')->first();

        if (!$record) {
            return null;
        }

        return json_decode($record->value);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::IS_ARRAY, 'Desired state'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}