<?php
/**
 * Created by PhpStorm.
 * User: Łukasz Biały
 * URL: http://octexchange.eu
 * Date: 8/13/15
 * Time: 2:17 AM
 */

namespace OctExchange\Spawn\Console;

use Illuminate\Console\Command;
use OctExchange\Spawn\Classes\MotherConnector;
use OctExchange\Spawn\Classes\PluginInstaller;
use OctExchange\Spawn\Classes\ProjectUpdater;
use OctExchange\Spawn\Models\Settings;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class UpdateProject
 *
 * @package OctExchange\Spawn\Console
 */
class UpdateTheme extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'exchange:update-theme';

    /**
     * The console command description.
     */
    protected $description = 'Triggers Exchange Project Sync';

    public function handle()
    {
        $this->fire();
    }


    /**
     * Execute the console command.
     *
     * @throws \InvalidArgumentException
     * @throws \ApplicationException
     */
    public function fire()
    {
        $slug = $this->argument('slug');
        $settings = Settings::instance();
        $this->info('ExchangeUpdate started');
        $this->info('');
        $projectKey = $settings->get('project_key');
        $ssl = $settings->checkForceSsl();
        if (!$projectKey) {
            throw new \ApplicationException(\Lang::trans('octexchange.spawn::lang.errors.no_project_key'));
        }
        if ($ssl) {
            $this->info('Connection:    secure');
        } else {
            $this->warn('Connection:    unsecure');
        }
        $conn = new MotherConnector($ssl);
        $projectUpdater = new ProjectUpdater($conn, $projectKey);
        $this->info('Upstream:      '.$projectUpdater->getMother());
        $this->info('');
        $this->comment('Starting update, this may take a moment...');
        $projectUpdater->updateTheme($slug);
        $this->info('Update finished.');
    }
    /**
     * Get the console command arguments.
     */
    protected function getArguments()
    {
        return [
            [
                'slug',
                InputArgument::REQUIRED,
                'Theme slug',
            ],
        ];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions()
    {
        return [];
    }
}