<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/28/17
 * Time: 4:42 PM
 */

namespace OctExchange\Spawn\Console;

use Cms\Models\MaintenanceSetting;
use Illuminate\Console\Command;
use OctExchange\Spawn\Classes\MotherConnector;
use OctExchange\Spawn\Classes\ProjectUpdater;
use OctExchange\Spawn\Models\Settings;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RegisterProject extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'exchange:register';
    /**
     * @var string The console command description.
     */
    protected $description = 'Registers project key';

    public function handle(){
       $this->fire();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        // acTsZLsvOTiU4zuXnr5M
        $settings = Settings::instance();
        if($currentKey = $settings->project_key) {
            $this->warn(\Lang::trans('octexchange.spawn::lang.strings.current_key').': '.$currentKey);
        }
        $this->warn(\Lang::trans('octexchange.spawn::lang.strings.changing'));
        $newKey = $this->argument('project_key');
        $settings->project_key = $newKey;
        $settings->save();
        $ssl = $settings->checkForceSsl();
        $connector = new MotherConnector($ssl);
        $projectUpdater = new ProjectUpdater($connector, $newKey);
        $projectUpdater->init();
        $projectUpdater->updateProjectModel();

        $this->info(\Lang::trans('octexchange.spawn::lang.strings.project_key_set_to').': ' . $newKey);

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['project_key', InputArgument::REQUIRED, 'Desired state'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}