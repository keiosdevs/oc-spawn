<?php

return [
    'mothers'       => [
        env('EXCHANGE_MOTHER', 'mother.dev'),
    ],
    'deploy_type'   => env('EXCHANGE_DEPLOY_TYPE', 'production'), // Contact your plugin's provider for more details about this option.
    'version_check' => env('EXCHANGE_VERSION_CHECK', 0),
    'secure_mother' => env('EXCHANGE_SECURE', 0),
];