<?php
/**
 * MotherPlugin routes
 *
 * @plugin octexchange.motherplugin
 */
Route::group(
    ['prefix' => '/api/v1', 'middleware' => []],
    function () {
        Route::post(
            '/plugin/disable',
            function () {
                $controller = new \OctExchange\Spawn\Controllers\ApiController();

                return $controller->disablePlugin();
            }
        );
        Route::post(
            '/plugin/enable',
            function () {
                $controller = new \OctExchange\Spawn\Controllers\ApiController();

                return $controller->disablePlugin();
            }
        );
        Route::any(
            '/update',
            function () {
                $controller = new \OctExchange\Spawn\Controllers\ApiController();

                return $controller->update();
            }
        );

        Route::post(
            '/ping',
            function () {
                return response(['message' => 'OK'], 200);
            }
        );
    }
);