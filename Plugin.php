<?php namespace OctExchange\Spawn;

use Backend;
use Backend\Widgets\Form;
use OctExchange\Spawn\Classes\BackendPluginManager;
use OctExchange\Spawn\Classes\ConsoleOutputServiceProvider;
use OctExchange\Spawn\Console\EnableMaintaince;
use OctExchange\Spawn\Console\RegisterProject;
use OctExchange\Spawn\Console\UpdatePlugin;
use OctExchange\Spawn\Console\UpdatePlugins;
use OctExchange\Spawn\Console\UpdateProject;
use OctExchange\Spawn\Console\UpdateTheme;
use OctExchange\Spawn\Console\UpdateThemes;
use OctExchange\Spawn\Controllers\UpdateLogs;
use OctExchange\Spawn\Facades\ConsoleOutput;
use OctExchange\Spawn\Models\Settings;
use OctExchange\Spawn\Models\UpdateLog;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

/**
 * Spawn Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Spawn',
            'description' => 'Agent plugin for OctExchange.PluginMother',
            'author'      => 'OctExchange',
            'icon'        => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(ConsoleOutputServiceProvider::class);
        $this->registerUpdateLogs();
        $this->app->bind('ConsoleOutput', ConsoleOutput::class);
        $this->app->bind(
            'exchange:update',
            UpdateProject::class
        );
        $this->app->bind(
            'exchange:update-plugins',
            UpdatePlugins::class
        );
        $this->app->bind(
            'exchange:update-plugin',
            UpdatePlugin::class
        );
        $this->app->bind(
            'exchange:update-themes',
            UpdateThemes::class
        );
        $this->app->bind(
            'exchange:update-theme',
            UpdateTheme::class
        );
        $this->app->bind(
            'backend_plugin_manager',
            BackendPluginManager::class
        );
        $this->app->bind(
            'exchange:maint',
            EnableMaintaince::class
        );
        $this->app->bind(
            'exchange:register',
            RegisterProject::class
        );
        $this->commands(
            [
                'exchange:register',
                'exchange:update',
                'exchange:update-theme',
                'exchange:update-themes',
                'exchange:update-plugin',
                'exchange:update-plugins',
                'exchange:maint',
            ]
        );
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        $configPath = base_path().'/plugins/octexchange/spawn/config/config.php';
        $this->publishes([$configPath => config_path('/octexchange/spawn/config.php')], 'config');


        $this->app['events']->listen(
            'backend.form.extendFields',
            function (Form $form) {
                if (!$form->model instanceof Settings) {
                    return;
                }
                $controller = $form->getController();
                $myWidget = new BackendPluginManager($controller);
                $myWidget->alias = 'backend_plugin_manager';
                $myWidget->bindToController();
            }
        );
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'octexchange.spawn.settings' => [
                'tab'   => 'Spawn',
                'label' => 'Access to Settings',
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => \Lang::trans('octexchange.spawn::lang.settings.label'),
                'description' => \Lang::trans('octexchange.spawn::lang.settings.desc'),
                'category'    => \Lang::trans('octexchange.spawn::lang.settings.category'),
                'icon'        => 'icon-cog',
                'class'       => Settings::class,
                'order'       => 500,
                'permissions' => ['octexchange.spawn.settings'],
            ],
            /*
            'updatelogs' => [
                'label'       => \Lang::trans('octexchange.spawn::lang.updatelogs.label'),
                'description' => \Lang::trans('octexchange.spawn::lang.updatelogs.desc'),
                'category'    => SettingsManager::CATEGORY_LOGS,
                'url'         => Backend::url('system/updatelogs'),
                'permissions' => ['system.access_logs'],
                'order'       => 1000,
                'icon'        => 'icon-cog',
                'class'       => UpdateLogs::class,
             ],
            */
        ];
    }

    public function registerUpdateLogs()
    {
        SettingsManager::instance()->registerCallback(
            function ($manager) {
                $manager->registerSettingItems(
                    'October.System',
                    [
                        'update_logs' => [
                            'label'       => 'Update logs',
                            'description' => 'Exchange Update logs',
                            'category'    => SettingsManager::CATEGORY_LOGS,
                            'icon'        => 'icon-refresh',
                            'url'         => Backend::url('octexchange/spawn/updatelogs'),
                            'permissions' => ['system.access_logs'],
                            'order'       => 900,
                            'keywords'    => 'error exception',
                        ],
                    ]
                );
            }
        );
    }
}
