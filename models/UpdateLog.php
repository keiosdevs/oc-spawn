<?php namespace OctExchange\Spawn\Models;

use Model;

/**
 * UpdateLog Model
 */
class UpdateLog extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'octexchange_spawn_update_logs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $jsonable = ['update_details'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];



}
