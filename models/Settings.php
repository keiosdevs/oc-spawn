<?php namespace OctExchange\Spawn\Models;

use Cms\Classes\ThemeManager;
use Composer\Plugin\PluginManager;
use OctExchange\Spawn\Classes\MotherConnector;
use OctExchange\Spawn\ValueObjects\SettingsPluginEntry;
use Lang;
use Model;
use OctExchange\Spawn\ValueObjects\SettingsThemeEntry;
use System\Classes\PluginBase;
use System\Models\MailTemplate;
use System\Models\PluginVersion;

/**
 * Class Settings
 * @package OctExchange\Spawn\Models
 */
class Settings extends Model
{

    /**
     * @var \System\Classes\PluginManager
     */
    protected $manager;
    /**
     * @var array
     */
    public $implement = ['System.Behaviors.SettingsModel'];

    /**
     * @var string
     */
    public $settingsCode = 'pluginspawn_settings';
    /**
     * @var string
     */
    public $settingsFields = 'fields.yaml';

    /**
     * @return bool
     */
    public function checkForceSsl()
    {
        $config = \Config::get('octexchange.spawn::secure_mother');
        if ($config) {
            return true;
        }

        return $this->secure_mother;
    }

    /**
     * @return array
     * @throws \OctExchange\Spawn\Exceptions\ExchangeException
     */
    public function getProjectTableContent()
    {
        $result = [];
        if (!$this->project_key) {
            return $result;
        }
        $ssl = $this->checkForceSsl();
        $connector = new MotherConnector($ssl);
        $response = $connector->authorizeProject($this->project_key);

        return $this->parseResponse($response);
    }

    /**
     * @param array $response
     * @return array
     */
    private function parseResponse($response)
    {
        $result = [];
        if (!array_key_exists('response', $response)) {
            return [];
        }
        if (!array_key_exists(200, $response['response'])) {
            return [];
        }
        if (!array_key_exists('details', $response['response'][200])) {
            return [];
        }
        $detailsRaw = $response['response'][200]['details'];
        $details = json_decode($detailsRaw, true);
        $result['project'] = $details['project'];
        $plugins = [];
        foreach ($details['plugins'] as $pluginSlug => $pluginDetails) {
            $plugin = $this->createPluginEntry($pluginDetails);
            $plugins[] = $plugin;
        }
        $result['themes'][] = $this->createThemeEntry($details['theme']);
        foreach ($details['themes'] as $themeSlug) {
            $result['themes'][] = $this->createThemeEntry($themeSlug);
        }

        $result['plugins'] = $plugins;

        return $result;
    }

    /**
     * @param $pluginDetails
     * @return SettingsThemeEntry
     */
    private function createThemeEntry($themeSlug)
    {
        $result = new SettingsThemeEntry();
        $result->slug = $themeSlug;
        $result->name = $themeSlug;
        $result->version = $this->getThemeVersion($themeSlug);

        $path = base_path('themes/'.$themeSlug);
        $yamlPath = $path.'/theme.yaml';
        if (is_file($yamlPath)) {
            $yaml = \Yaml::parse(file_get_contents($yamlPath));
            if (array_key_exists('author', $yaml)) {
                $result->author = $yaml['author'];
            }
            if (array_key_exists('name', $yaml)) {
                $result->name = $yaml['name'];
            }
            if (array_key_exists('description', $yaml)) {
                $result->description = $yaml['description'];
            }
        }

        return $result;
    }

    /**
     * @param $pluginDetails
     * @return SettingsPluginEntry
     */
    private function createPluginEntry($pluginDetails)
    {
        $alias = $pluginDetails['namespace'].'.'.$pluginDetails['directory'];
        $plugin = new SettingsPluginEntry();
        $plugin->alias = $alias;
        $plugin->name = $pluginDetails['name'];
        $plugin->slug = $pluginDetails['slug'];
        $plugin->local_version = $this->getVersion($alias);
        $plugin->upstream_version = $pluginDetails['version'];

        $plugin->findState();
        $plugin->dependencies = $this->findDependenciesDetails($plugin);

        return $plugin;
    }

    /**
     * We need to have it here because PluginVersion tells fakes
     *
     * @param $alias
     * @return string
     */
    private function getThemeVersion($alias)
    {
        $path = base_path('themes/'.$alias);
        $yamlPath = $path.'/version.yaml';

        if (!is_file($yamlPath)) {
            return '1.0.0';
        }

        if (!is_file($yamlPath)) {
            return 'YAML NOT FOUND';
        }
        $yaml = \Yaml::parse(file_get_contents($yamlPath));

        $versions = array_keys($yaml);

        return end($versions);
    }

    /**
     * We need to have it here because PluginVersion tells fakes
     *
     * @param $alias
     * @return string
     */
    private function getVersion($alias)
    {
        $pluginManager = \System\Classes\PluginManager::instance();
        $path = $pluginManager->getPluginPath($alias);
        $yamlPath = $path.'/updates/version.yaml';
        if (!is_file($yamlPath)) {
            $yamlPath = $this->getPathManually($alias);
        }

        if (!is_file($yamlPath)) {
            return 'YAML NOT FOUND';
        }
        $yaml = \Yaml::parse(file_get_contents($yamlPath));
        $versions = array_keys($yaml);

        return end($versions);
    }

    private function getPathManually($alias)
    {
        $exploded = explode('.', $alias);
        if (count($exploded) !== 2) {
            return null;
        }
        $namespace = strtolower($exploded[0]);
        $pluginName = strtolower($exploded[1]);

        return base_path('plugins/'.$namespace.'/'.$pluginName.'/updates/version.yaml');
    }

    /**
     * @param $plugin
     * @return array
     */
    private function findDependenciesDetails($plugin)
    {
        $result = [];
        /** @var \System\Classes\PluginManager $manager */
        $this->manager = \System\Classes\PluginManager::instance();
        $deps = $this->manager->getDependencies($plugin->alias);
        $localPlugins = $this->manager->getPlugins();
        if ($deps) {
            foreach ($deps as $depAlias) {

                if (array_key_exists($depAlias, $localPlugins)) {
                    $dep = $this->createExistingPlugin($depAlias);
                } else {
                    $dep = $this->createMissingPlugin($depAlias);
                }
                $result[] = $dep;
            }
        }

        return $result;
    }

    /**
     * @param $alias
     * @return SettingsPluginEntry
     */
    private function createExistingPlugin($alias)
    {
        $dep = new SettingsPluginEntry();
        $namespace = $this->getNamespace($alias);
        $directory = $this->getDirectory($alias);
        $dep->state = 'Installed';
        $dep->alias = $alias;
        $dep->name = $namespace.' '.$directory;
        $dep->is_disabled = $this->manager->isDisabled($alias);
        if ($dep->is_disabled) {
            $dep->state = trans('octexchange.spawn::lang.strings.disabled');
        }

        return $dep;
    }

    /**
     * @param $alias
     * @return SettingsPluginEntry
     */
    private function createMissingPlugin($alias)
    {

        $dep = new SettingsPluginEntry();
        $namespace = $this->getNamespace($alias);
        $directory = $this->getDirectory($alias);
        $dep->state = trans('octexchange.spawn::lang.strings.unavailable');
        $dep->alias = $alias;
        $dep->name = $namespace.' '.$directory;
        $dep->is_disabled = $this->manager->isDisabled($alias);

        return $dep;
    }

    /**
     * @param $alias
     * @return mixed
     */
    private function getNamespace($alias)
    {
        $array = explode('.', $alias);

        return $array[0];
    }

    /**
     * @param $alias
     * @return mixed
     */
    private function getDirectory($alias)
    {
        $array = explode('.', $alias);

        return $array[1];
    }

}