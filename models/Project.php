<?php namespace OctExchange\Spawn\Models;

use Model;

/**
 * Project Model
 */
class Project extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'octexchange_spawn_projects';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * Invalidate cache
     */
    public function afterSave()
    {
        \Cache::forget('instance_project');
    }
}
