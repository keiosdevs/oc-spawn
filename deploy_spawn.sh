#!/bin/bash
instance_name=$1
project_key=$2

function installOctober {
    instance_name=$1
    composer create-project october/october $instance_name dev-develop
    cd $instance_name
    mkdir plugins/octexchange
    cd plugins/octexchange/
    git clone -b develop https://octexchangeweb@bitbucket.org/octexchangedevs/oc-spawn.git spawn
    cd ../..
    composer update
    php artisan vendor:publish
    echo "Configure your database in config/database.php and press enter"
    read
    echo "Registering permissions..."
    chown http:http * -R
    echo "Migrating database..."
    php artisan october:up
    echo "Registering Exchange Project..."
    php artisan exchange:register $project_key
    echo "Updating Exchange Plugins..."
    php artisan exchange:update
}

if [ -z "$instance_name" ];
then
    echo "usage: deploy-spawn.sh <name> <project_key>"
    exit 1;
fi
if [ -z "$project_key" ];
then
    # probably assign some shared project_key or whatever
    echo "usage: deploy-spawn.sh <name> <project_key>"
    exit 1;
fi
installOctober $instance_name

