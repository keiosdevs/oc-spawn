<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/28/17
 * Time: 3:51 PM
 */

namespace OctExchange\Spawn\Exceptions;

use October\Rain\Exception\ApplicationException;

/**
 * Class ExchangeException
 *
 * @package OctExchange\PluginMother\Exceptions
 */
class ExchangeException extends ApplicationException
{

}