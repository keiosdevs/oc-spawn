<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 2017-02-27
 * Time: 13:00
 */

namespace OctExchange\Spawn\Repositories;

use OctExchange\Spawn\Models\Project;

/**
 * Class ProjectRepository
 * @package OctExchange\Spawn\Repositories
 */
class ProjectRepository
{
    /**
     * There should be only one project at one time with id 1 in 1.0 version.
     * @return Project
     */
    public function getProject()
    {
        return Project::where('id', 1)->rememberForever('instance_project')->first();
    }
}