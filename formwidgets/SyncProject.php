<?php namespace OctExchange\Spawn\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Backend\Facades\Backend;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use OctExchange\Spawn\Classes\MotherConnector;
use OctExchange\Spawn\Classes\PluginInstaller;
use OctExchange\Spawn\Classes\ProjectUpdater;
use OctExchange\Spawn\Classes\RequestSender;
use OctExchange\Spawn\Models\Settings;

/**
 * SyncProject Form Widget
 */
class SyncProject extends FormWidgetBase
{
    /**
     * {@inheritDoc}
     */
    protected $defaultAlias = 'ke_syncproject';

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        return $this->makePartial('syncproject');
    }

    public function onSyncProject()
    {
        $settings = Settings::instance();
        $projectKey = $settings->get('project_key');
        $ssl = $settings->checkForceSsl();
        if (!$projectKey) {
            throw new \ApplicationException(
                \Lang::trans('octexchange.spawn::lang.errors.enter_and_save_project_key_first')
            );
        }

        $conn = new MotherConnector($ssl);
        $installer = new PluginInstaller();
        $response = $conn->authorizeProject($projectKey);
        $projectUpdater = new ProjectUpdater($conn, $projectKey);
        $projectUpdater->init();
        $projectUpdater->updateProjectModel();

        try {
            $installer->syncInstance($conn, $projectKey, $response);
            \Flash::success('Project updated successfully!');

            return \Redirect::to(\Backend::url('system/settings/update/octexchange/spawn/settings'));
        } catch (\Exception $e) {
            \Log::error(
                'UPDATE ERROR! For better debugging try artisan exchange:update --verbose '.PHP_EOL.PHP_EOL.$e->getMessage(
                ).' '.$e->getTraceAsString()
            );
            \Flash::error('Error happened during the update. Details are available in the Event Log.');

            return \Redirect::to(\Backend::url('system/settings/update/octexchange/spawn/settings'));
        }

    }

    /**
     * {@inheritDoc}
     */
    public function getSaveValue($value)
    {
        return null;
    }

}
