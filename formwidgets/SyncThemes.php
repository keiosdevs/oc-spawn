<?php namespace OctExchange\Spawn\FormWidgets;

use Backend\Classes\FormWidgetBase;
use OctExchange\Spawn\Classes\ProjectUpdater;
use OctExchange\Spawn\Models\Project;
use OctExchange\Spawn\Classes\MotherConnector;
use OctExchange\Spawn\Classes\ThemeInstaller;
use OctExchange\Spawn\Models\Settings;

/**
 * SyncProject Form Widget
 */
class SyncThemes extends FormWidgetBase
{
    /**
     * {@inheritDoc}
     */
    protected $defaultAlias = 'ke_syncthemes';

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        return $this->makePartial('syncthemes');
    }

    public function onSyncThemes()
    {
        /** @var Settings $settings */
        $settings = Settings::instance();
        $projectKey = $settings->get('project_key');
        $ssl = $settings->checkForceSsl();
        if (!$projectKey) {
            throw new \ApplicationException(
                \Lang::trans('octexchange.spawn::lang.errors.enter_and_save_project_key_first')
            );
        }

        $conn = new MotherConnector($ssl);
        /** @var ThemeInstaller $installer */
        $installer = new ThemeInstaller();

        $project = Project::where('project_key', $projectKey)->first();
        $response = $conn->authorizeProject($projectKey);
        $projectUpdater = new ProjectUpdater($conn, $projectKey);
        $projectUpdater->init();
        $projectUpdater->updateProjectModel();

        try {
            $installer->updateThemes($conn, $project, $response['mother']);
            \Flash::success('Project updated successfully!');

            return \Redirect::to(\Backend::url('system/settings/update/octexchange/spawn/settings'));
        } catch (\Exception $e) {
            \Log::error(
                'UPDATE ERROR! For better debugging try artisan exchange:update --verbose '.PHP_EOL.PHP_EOL.$e->getMessage(
                ).' '.$e->getTraceAsString()
            );
            \Flash::error('Error happened during the update. Details are available in the Event Log.');

            return \Redirect::to(\Backend::url('system/settings/update/octexchange/spawn/settings'));
        }

    }

    /**
     * {@inheritDoc}
     */
    public function getSaveValue($value)
    {
        return null;
    }

}
