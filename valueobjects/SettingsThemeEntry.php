<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/15/17
 * Time: 11:53 AM
 */

namespace OctExchange\Spawn\ValueObjects;

/**
 * Class SettingsThemeEntry
 * @package OctExchange\Spawn\ValueObjects
 */
class SettingsThemeEntry
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $author = '';
    /**
     * @var string
     */
    public $description = '';
    /**
     * @var string
     */
    public $slug;
    /**
     * @var string
     */
    public $version;
}