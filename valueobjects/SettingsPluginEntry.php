<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/20/17
 * Time: 8:04 PM
 */

namespace OctExchange\Spawn\ValueObjects;

use System\Classes\PluginManager;

/**
 * Class SettingsPluginEntry
 * @package OctExchange\Spawn\ValueObjects
 */
class SettingsPluginEntry
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $slug;

    /**
     * @var string
     */
    public $alias;

    /**
     * @var string
     */
    public $state;
    /**
     * @var string
     */
    public $local_version;
    /**
     * @var string
     */
    public $upstream_version;

    /**
     * @var array
     */
    public $dependencies = [];

    /**
     * @var bool
     */
    public $is_disabled = false;

    /**
     *
     */
    public function findState()
    {
        if (!$this->local_version) {
            $this->state = trans('octexchange.spawn::lang.strings.unavailable');
        }
        if (!$this->upstream_version) {
            $this->state = trans('octexchange.spawn::lang.strings.unavailable');
        }
        if ($this->local_version !== $this->upstream_version) {
            $localInt = (int)str_replace('.','',$this->local_version);
            $upstreamInt = (int)str_replace('.','',$this->local_version);
            if($localInt < $upstreamInt){
                $this->state = trans('octexchange.spawn::lang.strings.to_update');
            } else {
                $this->state = trans('octexchange.spawn::lang.strings.latest');
            }
        }

        if ($this->local_version === $this->upstream_version) {
            $this->state = trans('octexchange.spawn::lang.strings.latest');
        }

        $manager = PluginManager::instance();
        $this->is_disabled = $manager->isDisabled($this->alias);
        if ($this->is_disabled) {
            $this->state = trans('octexchange.spawn::lang.strings.disabled');
        }
    }
}