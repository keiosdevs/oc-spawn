<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/17/17
 * Time: 1:30 AM
 */

namespace OctExchange\Spawn\Classes;

use Carbon\Carbon;
use OctExchange\Spawn\Facades\ConsoleOutput;
use OctExchange\Spawn\Models\Settings;
use System\Classes\PluginManager;
use OctExchange\Spawn\Models\Project;
use Composer\Console\Application as ComposerApp;
use OctExchange\Spawn\Models\UpdateLog;
use Symfony\Component\Console\Input\ArrayInput;
use System\Classes\UpdateManager;
use System\Models\PluginVersion;

/**
 * Class PluginInstaller
 *
 * @package OctExchange\Spawn\Classes
 */
class PluginInstaller extends Installer
{
    /**
     * @var int
     */
    private $update = 0;
    /**
     * @var string
     */
    private $mother;
    /**
     * @var RequestSender
     */
    private $requestSender;
    /**
     * @var FileTools
     */
    private $fileTools;
    /**
     * @var ZipTools
     */
    private $zipTools;

    /**
     * @var bool
     */
    private $init = false;

    /**
     * @var array
     */
    private $log = [];

    /**
     * @var bool
     */
    private $forceComposer = false;

    /**
     * @var array
     */
    private $toUpdate = [];

    /**
     * @var bool
     */
    private $versionCheck;

    /**
     * PluginInstaller constructor.
     */
    public function __construct()
    {
        $this->versionCheck = \Config::get('octexchange.spawn::version_check');
        $this->requestSender = new RequestSender();
        $this->fileTools = new FileTools();
        $this->zipTools = new ZipTools();
        $settings = Settings::instance();
        $this->requestSender->setForceSsl($settings->checkForceSsl());
    }

    /**
     * @param $response
     * @return bool|int
     */
    public function updateMarketPlugins($response)
    {
        $this->consoleEmptyLine();
        $this->pushToConsole('Installing Official Market plugins...', 'info');

        if (!array_key_exists('pre', $response)) {
            return false;
        }
        //       ConsoleOutput::writeln('<info>Upgrading October...</info>');
        //       \Artisan::call('october:update');
        //       ConsoleOutput::writeln(\Artisan::output());
        $pre = $response['pre'];
        $count = 0;
        foreach ($pre as $alias) {
            $version = PluginVersion::getVersion($alias);
            if ($version) {
                ++$count;
                $this->pushToConsole($alias.' has already been installed, skipping', 'comment');
            } else {
                ++$count;
                $this->pushToConsole('Installing '. $alias, 'comment');
                \Artisan::call('plugin:install', ['name' => $alias]);
            }

            // fucking retarded
            $nameArray = explode('.', $alias);
            $pluginPath = base_path('plugins/'.strtolower($nameArray[0]).'/'.strtolower($nameArray[1]));
            $vendor = $pluginPath.'/vendor';
            if (is_dir($vendor)) {
                $this->fileTools->rmdir($vendor);
            }
        }
        $this->pushToConsole('October Market dependencies installed', 'info');
        $this->consoleEmptyLine();

        return $count;
    }

    /**
     * @param MotherConnector $connector
     * @param string          $slug
     * @param Project         $project
     * @param string          $mother
     * @param bool            $forceComposer
     * @return array
     */
    public function updateSinglePlugin(MotherConnector $connector, $slug, Project $project, $mother, $forceComposer = false)
    {
        /**
         * Here we get project plugins array and set some global defaults
         *
         * @var array $plugins
         */
        $this->forceComposer = $forceComposer;
        $this->mother = $mother;
        $this->toUpdate = [];
        $notificationData = [
            'status' => 'success',
            'errors' => [],
        ];
        $deployType = \Config::get('octexchange.spawn::deploy_type');
        $plugins = json_decode($project->plugins, true);
        $branchMap = $this->prepareBranchMap($connector->getBranches($mother, $project, $deployType));
        $this->purgeOldTemps($plugins);

        /**
         * Iteration trough plugins starts
         */
        foreach ($plugins as $key => $pluginData) {
            try {
                if($pluginData['slug'] == $slug) {
                    $this->updatePlugin($pluginData, $branchMap, $project);
                }
            } catch (\Exception $e) {
                \Log::error('EXCHANGE ERROR! '.$e->getMessage().' '.$e->getTraceAsString());
                $this->pushToLog('Update failed with '.$e->getMessage(), 'error');
                $notificationData['status'] = 'failed';
                $notificationData['errors'][] = $pluginData['name'].': '.$e->getMessage();
                if (\Config::get('app.debug')) {
                    dump($e->getMessage().' '.$e->getTraceAsString());
                }
            }
        }
        /**
         * If at least one plugin has been updated, we run composer and october update.
         */
        if ($this->forceComposer || count($this->toUpdate) > 0) {
            $this->finishPluginInstallation($connector, $notificationData, $project);
            \Flash::success('Update finished!');
        } else {
            $this->pushToLog('No update required', 'comment');
        }

        return $this->log;
    }

    /**
     * @param $branchesResponse
     * @return array|mixed
     */
    private function prepareBranchMap($branchesResponse)
    {
        $branchMap = [];
        if (array_key_exists(200, $branchesResponse)) {
            $branchMap = json_decode($branchesResponse[200]['details'], true);
        }
        $this->pushToLog('Branch map loaded', 'info');
        $this->consoleEmptyLine();

        return $branchMap;
    }

    /**
     * @param $plugins
     * @throws \ApplicationException
     */
    private function purgeOldTemps($plugins)
    {
        if ($plugins && is_dir(base_path().'storage/temp/backup/')) {
            $this->fileTools->rmdir(base_path().'storage/temp/backup/');
        }
    }

    /**
     * This is fucking crazy method, so added some more comments here
     *
     * @param MotherConnector $connector
     * @param Project         $project
     * @param string          $mother
     * @param bool            $forceComposer
     * @return array
     * @throws \ApplicationException
     */
    public function updatePlugins(MotherConnector $connector, Project $project, $mother, $forceComposer = false)
    {
        /**
         * Here we get project plugins array and set some global defaults
         *
         * @var array $plugins
         */
        $this->forceComposer = $forceComposer;
        $this->mother = $mother;
        $this->toUpdate = [];
        $notificationData = [
            'status' => 'success',
            'errors' => [],
        ];
        $deployType = \Config::get('octexchange.spawn::deploy_type');
        $plugins = json_decode($project->plugins, true);
        $branchMap = $this->prepareBranchMap($connector->getBranches($mother, $project, $deployType));
        $this->purgeOldTemps($plugins);

        /**
         * Iteration trough plugins starts
         */
        foreach ($plugins as $key => $pluginData) {
            try {
                $this->updatePlugin($pluginData, $branchMap, $project);
            } catch (\Exception $e) {
                \Log::error('EXCHANGE ERROR! '.$e->getMessage().' '.$e->getTraceAsString());
                $this->pushToLog('Update failed with '.$e->getMessage(), 'error');
                $notificationData['status'] = 'failed';
                $notificationData['errors'][] = $pluginData['name'].': '.$e->getMessage();
                if (\Config::get('app.debug')) {
                    dump($e->getMessage().' '.$e->getTraceAsString());
                }
            }
        }
        /**
         * If at least one plugin has been updated, we run composer and october update.
         */
        if ($this->forceComposer || count($this->toUpdate) > 0) {
            $this->finishPluginInstallation($connector, $notificationData, $project);
            \Flash::success('Update finished!');
        } else {
            $this->pushToLog('No update required', 'comment');
        }

        return $this->log;
    }

    /**
     * @param $pluginData
     * @param $branchMap
     * @param $project
     * @throws \ApplicationException
     */
    public function updatePlugin($pluginData, $branchMap, $project)
    {
        /**
         * Variables are set
         */
        $branchCode = 'master';
        $slug = $pluginData['slug'];
        if (array_key_exists($slug, $branchMap)) {
            $branchCode = $branchMap[$slug];
        }
        $namespaceDir = strtolower($pluginData['namespace']);
        $pluginDir = strtolower($pluginData['directory']);
        $pluginName = $pluginData['name'];
        $alias = $pluginData['namespace'].'.'.$pluginData['directory'];
        $pluginPath = $this->fileTools->getPluginPath(['namespace' => $namespaceDir], null, $pluginDir);
        $namespacePath = $this->fileTools->getNamespacePath($namespaceDir);
        $pluginExists = is_dir($pluginPath) && file_exists($pluginPath.'/Plugin.php');
        $namespaceExists = is_dir($namespacePath);
        $this->update = true;
        /**
         * We compare either zip file sizes or versions from version.yaml, depends on version_check config field.
         */
        $zipPath = $this->processPluginZip($pluginData, $project, $branchCode, $alias);

        /**
         * Zip either does not exist or size differs - we download new ZIP
         */
        if ($this->update) {
            $zipPath = $this->downloadZip($pluginData['slug'], $project->project_key, $this->mother, $branchCode);
            $this->pushToLog('Zip downloaded.', 'info');
            $this->toUpdate[] = $pluginData['namespace'].$pluginData['directory'];
        }
        /**
         * If it's first plugin from the namespace, create namespace directory
         */
        if (!$namespaceExists) {
            $this->fileTools->mkdir($namespacePath);
            $this->pushToLog("$namespacePath directory created", 'comment');
        }
        /**
         * If plugin does not exist, we create plugin directory and proceed to Installation
         */
        if (!$pluginExists) {
            $this->fileTools->mkdir($pluginPath);
            $this->pushToLog("$pluginPath directory created", 'comment');
            $this->installPlugin($pluginData, $zipPath);
            $this->pushToLog("$pluginName plugin installed", 'info');
            $this->consoleEmptyLine();
            $als = $pluginData['namespace'].$pluginData['directory'];
            if (!array_key_exists($als, $this->toUpdate)) {
                $this->toUpdate[] = $als;
            }
            /**
             *  and if does exist and we noticed new ZIP, we proceed to update
             */
        } elseif ($this->update) {
            $this->overwritePlugin($pluginData, $zipPath);
            $this->pushToLog("$pluginName plugin updated", 'info');
            $this->consoleEmptyLine();
        }
    }

    /**
     * @param string $alias
     * @param string $projectKey
     * @param string $mother
     *
     * @return bool
     * @throws \ApplicationException
     */
    private function checkVersion($alias, $projectKey, $mother)
    {
        $update = true;
        $version = PluginVersion::getVersion($alias);
        if (!$version) {
            $this->pushToLog(
                $alias.': Alias not found. Zip will be downloaded.',
                'comment'
            );
            $update = true;
        }
        $remoteVersion = $this->getRemoteVersion($alias, $projectKey, $mother);
        if ($version === $remoteVersion) {
            $this->pushToLog(
                $alias.': Local version: '.$version.' Remote version: '.$remoteVersion.' - skippping',
                'comment'
            );
            $update = false;
        }

        return $update;
    }

    /**
     * @param string $alias
     * @param string $projectKey
     * @param string $mother
     *
     * @return string
     * @throws \October\Rain\Exception\ApplicationException
     * @throws \ApplicationException
     */
    private function getRemoteVersion($alias, $projectKey, $mother)
    {
        $deployType = \Config::get('octexchange.spawn::deploy_type');
        $response = $this->requestSender->sendPostRequest(
            [
                'alias'       => $alias,
                'project_key' => $projectKey,
                'deploy_type' => $deployType,
            ],
            $mother.'/api/v1/plugin/version'
        );
        if (!array_key_exists(200, $response)) {
            throw new \ApplicationException(
                'Error with connecting to upstream for version check: '.max(array_keys($response))
            );
        }
        $details = json_decode($response[200]['details'], true);

        return $details['version'];

    }

    /**
     * @param string $slug
     * @param string $projectKey
     * @param string $mother
     * @param string $branchCode
     *
     * @return string
     * @throws \October\Rain\Exception\ApplicationException
     * @throws \ApplicationException
     */
    public function downloadZip($slug, $projectKey, $mother, $branchCode)
    {
        $deployType = \Config::get('octexchange.spawn::deploy_type');
        $response = $this->requestSender->sendPostRequest(
            [
                'project_key' => $projectKey,
                'slug'        => $slug,
                'deploy_type' => $deployType,
            ],
            $mother.'/api/v1/get-plugin'
        );

        if (!is_dir(temp_path().'/'.$branchCode)) {
            $this->fileTools->mkdir(temp_path().'/'.$branchCode);
        }
        if (!array_key_exists(200, $response)) {
            throw new \ApplicationException(\Lang::trans('octexchange.spawn::lang.errors.error_downloading_plugin'));
        }
        $zipPath = temp_path().'/'.$branchCode.'/'.$slug.'.zip';
        if (is_file($zipPath)) {
            unlink($zipPath);
        }
        $coreResponse = $response[200];
        $stream = $coreResponse['details'];
        $file = fopen($zipPath, 'w+');
        fwrite($file, $stream);
        fclose($file);

        return $zipPath;
    }

    /**
     * This should be replaced by version.yaml comparison
     *
     * @param string $zipPath
     * @param array  $pluginData
     * @param string $mother
     *
     * @param        $projectKey
     * @param        $branchCode
     * @return bool
     * @throws \October\Rain\Exception\ApplicationException
     * @throws \ApplicationException
     */
    public function checkLocalZip($zipPath, $pluginData, $mother, $projectKey, $branchCode)
    {
        $deployType = \Config::get('octexchange.spawn::deploy_type');

        if (!is_file($zipPath)) {
            $this->downloadZip($pluginData['slug'], $projectKey, $mother, $branchCode);
        }
        $localSize = filesize($zipPath);
        $remoteSize = 0;

        $checkSizeResponse = $this->requestSender->sendPostRequest(
            ['slug' => $pluginData['slug'], 'project_key' => $projectKey, 'deploy_type' => $deployType],
            $mother.'/api/v1/check-plugin'
        );
        if (array_key_exists(401, $checkSizeResponse)) {
            throw new \ApplicationException('Not authorised');
        }
        if ($checkSizeResponse && array_key_exists(200, $checkSizeResponse)) {
            $details = json_decode($checkSizeResponse[200]['details'], true);
            if (array_key_exists('size', $details)) {
                $remoteSize = $details['size'];
            }
        }
        $pluginName = $pluginData['name'];
        $update = $remoteSize !== $localSize;
        if ($update) {
            $this->pushToLog(
                $pluginName.': [local = '.$localSize.', remote = '.$remoteSize.'] - updating...',
                'info'
            );
        } else {
            $this->pushToLog(
                $pluginName.': [local = '.$localSize.', remote = '.$remoteSize.'] - skipping...',
                'comment'
            );
        }

        return $update;
    }

    /**
     * @param array  $data
     * @param string $zipPath
     *
     * @throws \ApplicationException
     */
    public function installPlugin(array $data, $zipPath)
    {
        /**
         * We define branch code (for directory), namespace, plugin name, it's internal October alias
         */
        $this->consoleEmptyLine();
        $this->pushToLog('Installing '.$data['slug'].'...', 'question');
        $branchCode = str_replace('/', '-', $data['git_branch']);
        $namespace = $data['namespace'];
        $pluginName = $data['directory'];
        $alias = $namespace.'.'.$pluginName;
        $namespacePath = $this->fileTools->getNamespacePath($namespace);
        /**
         * ...and two paths - one to extract zip and second to which the proper folder should be moved later.
         */
        $pluginPath = $namespacePath.'/'.strtolower($pluginName);
        if (is_dir($pluginPath)) {
            $this->fileTools->rmdir($pluginPath); // this is installation. we remove anything that may exists.
            $this->pushToConsole($pluginPath.' removed', 'comment');
        }
        $tempPath = temp_path().'/unzipped/'.strtolower($namespace).'/'.strtolower($pluginName);
        /**
         * Then we extract ZIP and move the plugin to its proper directory
         */
        $this->pushToConsole('Extracting to '.$tempPath.'...', 'comment');

        $this->zipTools->extract($zipPath, $tempPath);
        $this->pushToConsole('Extracted.', 'info');
        rename($tempPath.'/'.$branchCode, $pluginPath);
        $this->pushToConsole('Moved to '.$pluginPath, 'info');
        /**
         * and we can run plugin:refresh on the plugin
         */
        $manager = PluginManager::instance();
        $manager->loadPlugin(
            $namespace.'\\'.$pluginName,
            plugins_path().'/'.strtolower($namespace).'/'.strtolower($pluginName)
        );
        $this->pushToConsole('Plugin loaded', 'info');
    }

    /**
     * @param array  $data
     * @param string $zipPath
     *
     * @throws \ApplicationException
     */
    public function overwritePlugin($data, $zipPath)
    {
        $this->consoleEmptyLine();
        $this->pushToLog('Updating '.$data['slug'].'...', 'question');
        /**
         * We define paths
         */
        $namespacePath = $this->fileTools->getNamespacePath($data['namespace']);
        $pluginPath = $namespacePath.'/'.strtolower($data['directory']);
        $tempPath = $this->fileTools->getTempPath($data['slug']);
        /**
         * We move current plugin to backup
         */
        $this->pushToConsole('Moving current plugin to backup...', 'comment');
        $this->cleanDirectory($tempPath);
        rename($pluginPath, $tempPath);

        $this->pushToConsole('Moved.', 'info');
        $tempZipPath = temp_path().'/unzipped/'.strtolower($data['namespace']).'/'.strtolower($data['directory']);
        $branchCode = str_replace('/', '-', $data['git_branch']);
        /**
         * Then we extract ZIP and move the plugin to its proper directory
         */
        $this->pushToConsole('Extracting to '.$tempZipPath.'...', 'comment');
        $this->cleanDirectory($tempZipPath);
        $this->zipTools->extract($zipPath, $tempZipPath);
        $this->pushToConsole('Extracted.', 'info');
        $this->pushToConsole('Moving to '.$pluginPath.'...', 'comment');
        rename($tempZipPath.'/'.$branchCode, $pluginPath);
        $this->pushToConsole('Moved.', 'info');
        // TODO: this should be in settings
        $this->fileTools->rmdir($tempPath);
    }

    /**
     * @param $path
     */
    private function cleanDirectory($path)
    {
        if (is_dir($path)) {
            $this->fileTools->rmdir($path);
        }
        $this->fileTools->mkdir($path);
    }

    /**
     * Run composer
     */
    public function runComposer()
    {
        try {
            putenv('COMPOSER_HOME=/tmp');
            $oldLimit = ini_get('memory_limit');
            // this is ugly but there is no way composer filesystem will not reach above 150 mb
            ini_set('memory_limit', '-1');
            $input = new ArrayInput(['command' => 'update']);
            $composer = new ComposerApp();
            $composer->setAutoExit(false); // prevent `$application->run` method from exitting the script
            $composer->run($input);
            $this->pushToLog('Composer update finished', 'info');
            $input = new ArrayInput(['command' => 'dump-autoload']);
            $composer->run($input);
            $this->pushToLog('Composer dump autoload finished', 'info');
            // and we set it to old value as soon as it's finished
            ini_set('memory_limit', $oldLimit);
        } catch (\Exception $e) {
            \Log::error($e->getMessage().' '.$e->getTraceAsString());
            throw new \ApplicationException(
                'Problem with your composer. You either didnt initialize Spawn with composer update. Exact error has been logged'
            );
        }
    }

    /**
     * @param string $string
     * @param        $type
     */
    private function pushToLog($string, $type)
    {
        $this->pushToConsole($string, $type);

        $this->log[] = Carbon::now()->toDateTimeString().': '.$string;
    }

    /**
     * @return int
     */
    private function saveLog()
    {
        $updateLog = new UpdateLog();
        $updateLog->update_details = json_encode($this->log);
        $updateLog->save();

        return $updateLog->id;
    }

    /**
     * @param $slug
     * @param $project
     * @param $branchCode
     * @return string
     * @throws \ApplicationException
     */
    private function getPluginZipPath($slug, $project, $branchCode)
    {
        $zipPath = temp_path().'/'.$branchCode.'/'.$slug.'.zip';
        if (!is_file($zipPath)) {
            $zipPath = $this->downloadZip($slug, $project->project_key, $this->mother, $branchCode);
        }

        return $zipPath;
    }

    /**
     * @param $pluginData
     * @param $project
     * @param $branchCode
     * @param $alias
     * @return string
     * @throws \October\Rain\Exception\ApplicationException
     * @throws \ApplicationException
     */
    private function processPluginZip($pluginData, $project, $branchCode, $alias)
    {
        $zipPath = $this->getPluginZipPath($pluginData['slug'], $project, $branchCode);
        if ($this->versionCheck) {
            $this->update = $this->checkVersion($alias, $project->project_key, $this->mother);
        } else {
            if (is_file($zipPath)) {
                $this->update = $this->checkLocalZip(
                    $zipPath,
                    $pluginData,
                    $this->mother,
                    $project->project_key,
                    $branchCode
                );
            }
        }

        return $zipPath;
    }

    private function finishPluginInstallation($connector, $notificationData, $project)
    {
        $this->consoleEmptyLine();
        $this->pushToLog('Composer started...', 'comment');
        $this->runComposer();
        if ($this->init) {
            $this->runComposer(); // run it double time for initial deployment
        }
        $this->pushToLog('Composer finished', 'info');
        $this->consoleEmptyLine();
        $this->pushToLog('October:up started...', 'comment');
        $pluginManager = PluginManager::instance();
        $pluginManager->loadPlugins();
        $updateManager = UpdateManager::instance();
        $updateManager->update();
        $this->pushToLog('October:up finished', 'info');
        /**
         * Finally we push notification to Slack
         */
        $notificationMessage = count($this->toUpdate).' plugins updated on '.$project->alias;
        if ($notificationData['status'] === 'failed') {
            $notificationMessage = 'Update failed on '.$project->alias.' with errors: '.json_encode(
                    $notificationData['errors']
                );
        }
        $notificationData['updated_count'] = $this->toUpdate;
        $connector->notifyMother($project->project_key, $notificationMessage, $notificationData);
        $this->pushToLog('Mother notified.', 'info');
        $this->saveLog();
    }

}