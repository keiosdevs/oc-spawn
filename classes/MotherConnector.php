<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/17/17
 * Time: 1:18 AM
 */

namespace OctExchange\Spawn\Classes;

use OctExchange\Spawn\Exceptions\ExchangeException;
use OctExchange\Spawn\Facades\ConsoleOutput;
use October\Rain\Support\Facades\Config;

/**
 * Class MotherConnector
 *
 * @package OctExchange\Spawn\Classes
 */
class MotherConnector
{
    /**
     * @var array
     */
    public $mothers = [];

    /**
     * @var RequestSender
     */
    protected $requestSender;

    /**
     * @var string
     */
    public $mother;

    /**
     * MotherConnector constructor.
     * @param bool $ssl
     */
    public function __construct($ssl = false)
    {
        $this->requestSender = new RequestSender();
        if ($ssl) {
            $this->requestSender->setForceSsl(true);
        }
        $this->mothers = \Config::get('octexchange.spawn::mothers');
    }

    /**
     * @param string $projectKey
     * @param string $message
     * @param array  $additionalData
     *
     * @return array
     */
    public function notifyMother($projectKey, $message, array $additionalData = [])
    {
        $motherUrl = null;
        $response = [];
        $data = [
            'project_key' => $projectKey,
            'message'     => $message,
            'status'      => json_encode($additionalData),
        ];
        foreach ($this->mothers as $motherHost) {
            $motherUrl = $motherHost;
            $url = $motherHost.'/api/v1/status';

            $response = $this->requestSender->sendPostRequest($data, $url);

            if (array_key_exists(200, $response)) {
                $this->mother = $motherHost;
                break;
            }
        }

        return ['response' => $response, 'mother' => $motherUrl];
    }

    /**
     *
     * @param null|string $projectKey
     * @return array
     * @throws ExchangeException
     * @throws \ApplicationException
     */
    public function authorizeProject($projectKey = null)
    {
        $data = post();
        $deployType = \Config::get('octexchange.spawn::deploy_type');
        $data['deploy_type'] = $deployType;
        unset($data['_token'], $data['_session_key'], $data['Settings']);
        if ($projectKey) {
            $data['project_key'] = $projectKey;
        }
        if (!array_key_exists('project_key', $data)) {
            throw new \ApplicationException(\Lang::trans('octexchange.spawn::lang.errors.project_key_must_be_sent'));
        }

        $response = [];
        $motherUrl = null;
        $data['running_mode'] = PHP_SAPI;
        $data['domain'] = Config::get('app.url');

        foreach ($this->mothers as $motherHost) {
            $motherUrl = $motherHost;
            $url = $motherHost.'/api/v1/authorize';

            $response = $this->requestSender->sendPostRequest($data, $url);

            if (array_key_exists(200, $response)) {
                break;
            }
        }
        if (array_key_exists(402, $response)) {
            $details = $response[402]['details'];
            $details = json_decode($details, true);
            $message = 'Project disabled. Please contact support';
            if (array_key_exists('message', $details)) {
                $message = $details['message'];
            }
            throw new ExchangeException($message);
        }
        if (!$response) {
            throw new \ApplicationException(
                \Lang::trans('octexchange.spawn::lang.errors.cannot_download_data_from_upstream')
            );
        }
        if (PHP_SAPI === 'cli') {
            ConsoleOutput::writeln('<info>Authorisation: success</info>');
        }

        return ['response' => $response, 'mother' => $motherUrl];
    }

    /**
     * @param $mother
     * @param $project
     * @param $deployType
     *
     * @return array
     */
    public function getBranches($mother, $project, $deployType)
    {
        return $this->requestSender->sendPostRequest(
            [
                'project_key' => $project->project_key,
                'deploy_type' => $deployType,
            ],
            $mother.'/api/v1/project'
        );
    }

    /**
     * @param string $mother
     */
    public function addMother($mother)
    {
        $this->mothers[] = $mother;
    }

    public function getFirstMother()
    {
        if (count($this->mothers) === 0) {
            return null;
        }

        return $this->mothers[0];
    }

}