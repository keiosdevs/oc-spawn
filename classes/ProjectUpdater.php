<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/23/17
 * Time: 4:04 PM
 */

namespace OctExchange\Spawn\Classes;

use Carbon\Carbon;
use OctExchange\Spawn\Facades\ConsoleOutput;
use OctExchange\Spawn\Models\Project;
use OctExchange\Spawn\Models\UpdateLog;

/**
 * Class ProjectUpdater
 * @package OctExchange\Spawn\Classes
 */
class ProjectUpdater extends Installer
{

    /**
     * @var PluginInstaller
     */
    private $pluginInstaller;
    /**
     * @var ThemeInstaller
     */
    private $themeInstaller;
    /**
     * @var
     */
    private $project;
    /**
     * @var MotherConnector
     */
    private $connector;
    /**
     * @var bool
     */
    private $init;
    /**
     * @var string
     */
    private $projectKey;
    /**
     * @var array
     */
    private $response;
    /**
     * @var bool
     */
    private $forceComposer = false;
    /**
     * @var
     */
    private $updateStart;
    /**
     * @var
     */
    private $mother;
    /**
     * @var
     */
    private $projectAlias;
    /**
     * @var
     */
    private $plugins;
    /**
     * @var
     */
    private $theme;

    /**
     * ProjectUpdater constructor.
     * @param MotherConnector $connector
     * @param string          $projectKey
     * @param bool            $init
     */
    public function __construct(MotherConnector $connector, $projectKey, $init = false)
    {
        $this->connector = $connector;
        $this->pluginInstaller = new PluginInstaller();
        $this->themeInstaller = new ThemeInstaller();
        $this->init = $init;
        $this->projectKey = $projectKey;
    }

    /**
     *
     * @throws \OctExchange\Spawn\Exceptions\ExchangeException
     */
    public function init()
    {
        // variables and validation
        $this->response = $this->connector->authorizeProject($this->projectKey);
        ExchangeValidator::validateAuthentication($this->response);
        $this->updateStart = Carbon::now();
        $this->mother = $this->response['mother'];
        $this->response = $this->response['response'];
        ExchangeValidator::prevalidateResponse($this->response);
        $fullResponse = $this->response[200];
        $this->response = json_decode($fullResponse['details'], true);
        ExchangeValidator::validateResponse($this->response);
        $this->projectAlias = $this->response['project'];
        $this->plugins = $this->response['plugins'];
        $this->theme = $this->response['theme'];
        $this->project = $this->updateProjectModel();
        $this->pushToLog(count($this->plugins).' plugins found', 'comment');
    }

    /**
     *
     */
    public function updateAll()
    {
        $this->init();
        ExchangeValidator::validatePlugins($this->plugins);
        $this->enableMaintenance();

        // market plugins update
        $marketCount = $this->pluginInstaller->updateMarketPlugins($this->response);
        if ($marketCount > 0) {
            $this->forceComposer = true;
        }

        // update exchange plugins
        $this->pluginInstaller->updatePlugins($this->connector, $this->project, $this->mother, $this->forceComposer);

        // update exchange themes
        $this->consoleEmptyLine();
        $theme = str_replace('"', '', $this->project->theme);
        $themes = json_decode($this->project->themes, true);
        if ((!$theme) && count($themes) === 0) {
            $this->pushToConsole('Skipping themes (no themes assigned)...', 'comment');

            return null;
        }
        $this->themeInstaller->loadTheme($this->theme);
        $this->themeInstaller->loadProject($this->project);
        $this->themeInstaller->updateThemes($this->connector, $this->project, $this->mother);
        // maintaince disable
        $this->disableMaintenance(); // todo: remember to change if when above migrated to queues
        $this->summary();
    }

    /**
     *
     */
    public function updateThemes()
    {
        $this->init();
        $this->enableMaintenance();
        // update exchange themes
        $this->consoleEmptyLine();
        $this->themeInstaller->loadTheme($this->theme);
        $this->themeInstaller->loadProject($this->project);
        $theme = $this->project->theme;
        $themes = json_decode($this->project->themes, true);
        if (($theme === '' || $theme === null) && count($themes) === 0) {
            $this->pushToConsole('Skipping themes (no themes assigned)...','comment');
        } else {
            $this->themeInstaller->updateThemes($this->connector, $this->project, $this->mother);
        }
        // maintaince disable
        $this->disableMaintenance(); // todo: remember to change if when above migrated to queues
        $this->summary();
    }

    /**
     * @param $slug
     */
    public function updateTheme($slug)
    {
        $this->init();
        $this->enableMaintenance();
        // update exchange themes
        $this->consoleEmptyLine();
        $this->themeInstaller->loadTheme($this->theme);
        $this->themeInstaller->loadProject($this->project);
        $this->themeInstaller->updateSingleTheme($this->connector, $this->project, $slug, $this->mother);
        // maintaince disable
        $this->disableMaintenance(); // todo: remember to change if when above migrated to queues
        $this->summary();
    }

    /**
     *
     */
    public function updatePlugins()
    {
        $this->init();
        ExchangeValidator::validatePlugins($this->plugins);
        $this->enableMaintenance();

        // market plugins update
        $marketCount = $this->pluginInstaller->updateMarketPlugins($this->response);
        if ($marketCount > 0) {
            $this->forceComposer = true;
        }

        // update exchange plugins
        $this->pluginInstaller->updatePlugins($this->connector, $this->project, $this->mother, $this->forceComposer);

        // maintaince disable
        $this->disableMaintenance(); // todo: remember to change if when above migrated to queues
        $this->summary();
    }

    /**
     * @param string $plugin
     */
    public function updatePlugin($plugin)
    {
        $this->init();
        ExchangeValidator::validatePlugins($this->plugins);
        $this->enableMaintenance();

        // update exchange plugins
        $this->pluginInstaller->updateSinglePlugin(
            $this->connector,
            $plugin,
            $this->project,
            $this->mother,
            $this->forceComposer
        );

        // maintaince disable
        $this->disableMaintenance(); // todo: remember to change if when above migrated to queues
        $this->summary();
    }

    /**
     *
     */
    private function summary()
    {
        $this->consoleEmptyLine();

        // print summary
        $updateEnd = Carbon::now();
        $diff = $updateEnd->diffInSeconds($this->updateStart);
        $msgColor = 'error';
        if ($diff < 30) {
            $msgColor = 'comment';
        }
        if ($diff < 8) {
            $msgColor = 'info';
        }
        $this->pushToConsole('Maintenance mode disabled', 'info');
        $this->pushToLog('Downtime: '.$diff.' seconds', $msgColor);
        $this->saveLog();
        $this->consoleEmptyLine();
    }

    /**
     *
     */
    private function enableMaintenance()
    {
        \Artisan::call('exchange:maint');
        $this->consoleEmptyLine();
        $this->pushToConsole('Maintenance mode enabled', 'info');
        $this->consoleEmptyLine();
    }

    /**
     *
     */
    private function disableMaintenance()
    {
        \Artisan::call('exchange:maint');
        $this->consoleEmptyLine();
        $this->pushToConsole('Maintenance mode disabled', 'info');
        $this->consoleEmptyLine();
    }


    /**
     * @param string $string
     * @param        $type
     */
    private function pushToLog($string, $type)
    {
        $this->pushToConsole($string, $type);

        $this->log[] = Carbon::now()->toDateTimeString().': '.$string;
    }

    /**
     * @return mixed
     */
    public function getMother()
    {
        return $this->mother;
    }

    /**
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return int
     */
    private function saveLog()
    {
        $updateLog = new UpdateLog();
        $updateLog->update_details = json_encode($this->log);
        $updateLog->save();

        return $updateLog->id;
    }

    /**
     * @return Project
     */
    public function updateProjectModel()
    {
        $project = Project::where('project_key', $this->projectKey)->first();
        if (!$project) {
            $project = Project::where('id', 1)->first();
        }
        if (!$project) {
            $project = new Project();
            $project->id = 1;
        }
        $project->alias = $this->projectAlias;
        $project->project_key = $this->projectKey;
        $project->plugins = json_encode($this->response['plugins']);
        $project->theme = json_encode($this->response['theme']);
        $project->themes = json_encode($this->response['themes']);
        $project->save();

        return $project;
    }

}