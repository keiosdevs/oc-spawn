<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 2017-02-27
 * Time: 12:58
 */

namespace OctExchange\Spawn\Classes;

/**
 * Class FileTools
 *
 * @package OctExchange\Spawn\Classes
 */
class FileTools
{

    /**
     * Creates directory in a proper way.
     *
     * @param string $path
     *
     * @throws \ApplicationException
     */
    public function mkdir($path)
    {
        $parentDir = dirname($path);
        if (!@mkdir($parentDir) && !is_dir($parentDir)) {
            throw new \ApplicationException(\Lang::trans('octexchange.spawn::lang.errors.cannot_create_directory'));
        }
        if (!@mkdir($path) && !is_dir($path)) {
            throw new \ApplicationException(\Lang::trans('octexchange.spawn::lang.errors.cannot_create_directory'));
        }
    }

    /**
     * Removes directory recursively.
     *
     * @param string $path
     *
     * @throws \ApplicationException
     */
    public function rmdir($path)
    {
        if (!is_dir($path)) {
            throw new \ApplicationException(
                $path.' '.\Lang::trans('octexchange.spawn::lang.errors.must_be_a_directory')
            );
        }
        if ($path[strlen($path) - 1] !== '/') {
            $path .= '/';
        }
        //$files = glob($path.'*', GLOB_MARK);
        $files = glob($path.'{,.}[!.,!..]*', GLOB_MARK | GLOB_BRACE);
        foreach ($files as $file) {
            if (is_dir($file)) {
                $this->rmdir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($path);
    }

    /**
     * Provides temp path for backups for given slug
     *
     * @param string $slug
     *
     * @return string
     * @throws \ApplicationException
     */
    public function getTempPath($slug)
    {
        if (!is_dir(temp_path().'/backup/')) {
            $this->mkdir(temp_path().'/backup/');
        }

        return temp_path().'/backup/'.$slug;
    }

    /**
     * Provides plugins namespace path for given namespace
     *
     * @param string $namespace
     *
     * @return string
     */
    public function getNamespacePath($namespace)
    {
        return base_path().'/plugins/'.strtolower($namespace);
    }

    /**
     * Provides plugins path for given post array
     *
     * @param array $data
     * @param string|null $namespacePath
     * @param string|null $slug
     *
     * @return string
     */
    public function getPluginPath($data, $namespacePath = null, $slug = null)
    {
        if (!$namespacePath) {
            $namespacePath = $this->getNamespacePath($data['namespace']);
        }
        if (!$slug) {
            $slug = $this->getNamespacePath($data['directory']); //TODO
        }

        return $namespacePath.'/'.strtolower($slug);
    }

    public function getThemePath($slug)
    {
        return base_path().'/themes/'.strtolower($slug);
    }

}