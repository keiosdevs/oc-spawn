<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/17/17
 * Time: 1:30 AM
 */

namespace OctExchange\Spawn\Classes;

use Carbon\Carbon;
use Cms\Classes\Theme;
use Cms\Classes\ThemeManager;
use OctExchange\Spawn\Facades\ConsoleOutput;
use OctExchange\Spawn\Models\Settings;
use OctExchange\Spawn\Models\Project;
use Composer\Console\Application as ComposerApp;
use OctExchange\Spawn\Models\UpdateLog;
use October\Rain\Exception\ApplicationException;
use Symfony\Component\Console\Input\ArrayInput;

/**
 * Class PluginInstaller
 *
 * @package OctExchange\Spawn\Classes
 */
class ThemeInstaller extends Installer
{
    /**
     * @var RequestSender
     */
    private $requestSender;
    /**
     * @var FileTools
     */
    private $fileTools;
    /**
     * @var ZipTools
     */
    private $zipTools;

    /**
     * @var array
     */
    private $log = [];

    /**
     * @var MotherConnector
     */
    private $connector;

    /**
     * @var Project
     */
    private $project;

    /**
     * @var string
     */
    private $theme;

    /**
     * @var string
     */
    private $mother;

    /**
     * ThemeInstaller constructor.
     */
    public function __construct()
    {
        $this->requestSender = new RequestSender();
        $this->fileTools = new FileTools();
        $this->zipTools = new ZipTools();
        $settings = Settings::instance();
        $this->requestSender->setForceSsl($settings->checkForceSsl());
    }

    /**
     * @param $theme
     */
    public function loadTheme($theme)
    {
        $this->theme = $theme;
    }

    /**
     * @param $project
     */
    public function loadProject($project)
    {
        $this->project = $project;
    }

    public function init()
    {
        $this->pushToConsole('--------------------------------', 'comment');
        $this->pushToConsole('Updating theme...', 'comment');
        $this->consoleEmptyLine();
    }

    /**
     * @param MotherConnector $connector
     * @param Project         $project
     * @param                 $slug
     * @param                 $mother
     * @return array
     */
    public function updateSingleTheme(MotherConnector $connector, Project $project, $slug, $mother)
    {
        $this->init();
        $this->connector = $connector;
        $toUpdate = [];
        $deployType = \Config::get('octexchange.spawn::deploy_type');
        if (!$slug || $slug === '') {
            $this->pushToLog('No themes attached, skipping.', 'comment');

            return [];
        }

        $mainThemeUpdate = $this->updateTheme($slug, $project, $deployType, $mother);
        $toUpdate = array_merge($toUpdate, $mainThemeUpdate);
        /**
         * If at least one plugin has been updated, we run composer and october update.
         */
        if (count($toUpdate) > 0) {
            // TODO  theme boot_script
            \Flash::success('Update finished!');
        } else {
            $this->pushToLog('No update required', 'comment');
        }

        return $this->log;
    }

    /**
     * This is fucking crazy method, so added some more comments here
     *
     * @param MotherConnector $connector
     * @param Project         $project
     * @param string          $mother
     *
     * @return array
     * @throws \ApplicationException
     */
    public function updateThemes(MotherConnector $connector, Project $project, $mother)
    {
        $this->init();
        $this->connector = $connector;
        $toUpdate = [];
        $deployType = \Config::get('octexchange.spawn::deploy_type');
        $slug = $project->theme;
        if ((!$slug || $slug == '') && !$project->themes) {
            $this->pushToLog('No themes attached, skipping.', 'comment');

            return [];
        }

        $mainThemeUpdate = $this->updateTheme($slug, $project, $deployType, $mother);
        $toUpdate = array_merge($toUpdate, $mainThemeUpdate);
        if ($project->themes) {
            $themesArray = json_decode($project->themes, true);
            foreach ($themesArray as $additionalThemeSlug) {
                $addThemeUpdate = $this->updateTheme($additionalThemeSlug, $project, $deployType, $mother);
                /** @noinspection SlowArrayOperationsInLoopInspection - these are very small arrays */
                $toUpdate = array_merge($toUpdate, $addThemeUpdate);
            }
        }
        /**
         * If at least one plugin has been updated, we run composer and october update.
         */
        if (count($toUpdate) > 0) {
            // TODO  theme boot_script
            \Flash::success('Update finished!');
        } else {
            $this->pushToLog('No update required', 'comment');
        }

        return $this->log;
    }

    /**
     * @param string  $slug
     * @param Project $project
     * @param string  $deployType
     * @param string  $mother
     * @param bool    $isMain
     * @return array
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function updateTheme($slug, $project, $deployType, $mother, $isMain = false)
    {
        $themeName = $slug;
        $toUpdate = [];
        $notificationData = [
            'status' => 'success',
            'errors' => [],
        ];
        $branchesResponse = $this->requestSender->sendPostRequest(
            [
                'project_key' => $project->project_key,
                'deploy_type' => $deployType,
                'slug'        => $slug,
            ],
            $mother.'/api/v1/project/theme'
        );
        $branchMap = [];
        if (array_key_exists(200, $branchesResponse)) {
            $details = json_decode($branchesResponse[200]['details'], true);

            $themeName = $details['name'];
            $slug = $details['slug'];
            if (!$project->theme) {
                $project->theme = $slug;
                $project->save();
            }
            unset($details['name'], $details['slug']);
            $branchMap = $details;
        }
        if ($slug && is_dir(base_path().'storage/temp/backup/')) {
            $this->fileTools->rmdir(base_path().'storage/temp/backup/');
        }
        $this->pushToLog('Branch map loaded', 'info');
        $this->consoleEmptyLine();
        $this->pushToLog('Starting theme installation...', 'info');
        $this->consoleEmptyLine();
        $versionCheck = \Config::get('octexchange.spawn::version_check');
        $themeDir = strtolower($slug);
        try {
            /**
             * Variables are set
             */
            $branchCode = 'master';

            if (array_key_exists($slug, $branchMap)) {
                $branchCode = $branchMap[$slug];
            }
            $themePath = $this->fileTools->getThemePath($themeDir);
            $themeExists = is_dir($themePath) && file_exists($themePath.'/theme.yaml');
            $zipPath = temp_path().'/'.$branchCode.'/'.$slug.'.zip';
            $update = true;
            /**
             * We compare either zip file sizes or versions from version.yaml, depends on version_check config field.
             */
            if ($versionCheck) {
                if (!is_file($zipPath)) {
                    $zipPath = $this->downloadThemeZip($slug, $project->project_key, $mother, $branchCode);
                }
                $update = $this->checkThemeVersion(
                    $slug,
                    $themeName,
                    $zipPath,
                    $mother,
                    $project->project_key,
                    $branchCode
                );
            } else {
                if (is_file($zipPath)) {
                    $update = $this->checkLocalZip(
                        $slug,
                        $themeName,
                        $zipPath,
                        $mother,
                        $project->project_key,
                        $branchCode
                    );
                }
            }

            /**
             * Zip either does not exist or size differs - we download new ZIP
             */
            if ($update && $slug) {
                $zipPath = $this->downloadThemeZip($slug, $project->project_key, $mother, $branchCode);
                $this->pushToLog('Zip downloaded.', 'info');
                $toUpdate[] = $slug;
            }
            /**
             * If plugin does not exist, we create plugin directory and proceed to Installation
             */
            if (!$themeExists && $slug) {
                $this->fileTools->mkdir($themePath);
                $this->pushToLog("$themePath directory created", 'comment');
                $this->installTheme($slug, $branchCode, $zipPath);
                $this->pushToLog("$themeName theme installed", 'info');
                $this->consoleEmptyLine();

                if (!array_key_exists($slug, $toUpdate)) {
                    $toUpdate[] = $slug;
                }
                /**
                 *  and if does exist and we noticed new ZIP, we proceed to update
                 */
            } elseif ($update && $slug) {
                $this->proceedWithUpdate($slug, $branchCode, $zipPath);
                $this->pushToLog("$themeName theme updated", 'info');
                $this->consoleEmptyLine();
            }
        } catch (\Exception $e) {
            \Log::error('EXCHANGE ERROR! '.$e->getMessage().' '.$e->getTraceAsString());
            $this->pushToLog($themeName.' update failed with '.$e->getMessage(), 'error');
            $notificationData['status'] = 'failed';
            $notificationData['errors'][] = $themeName.': '.$e->getMessage();
            if (\Config::get('app.debug')) {
                dump($e->getTraceAsString());
            }
        }

        if (!$isMain) {
            $newTheme = Theme::load($themeDir);

            if (!$newTheme::exists($themeDir)) {
                $this->pushToConsole($themeDir.' does not exist!', 'comment');
            }

            if ($newTheme->isActiveTheme()) {
                $this->pushToConsole($themeDir.' is already active theme.', 'info');
            }

            $activeTheme = Theme::getActiveTheme();
            $from = $activeTheme ? $activeTheme->getId() : 'nothing';

            $this->pushToConsole('Switching theme from '.$from.' to '.$newTheme->getId(), 'comment');

            Theme::setActiveTheme($themeDir);

        }

        return $toUpdate;
    }

    /**
     * @param        $slug
     * @param        $themeName
     * @param        $zipPath
     * @param string $mother
     * @param string $projectKey
     * @param        $branchCode
     * @return bool
     * @throws \October\Rain\Exception\ApplicationException
     */
    private function checkThemeVersion($slug, $themeName, $zipPath, $mother, $projectKey, $branchCode)
    {
        /**
         * $update = true;
         * $version = PluginVersion::getVersion($alias);
         * if (!$version) {
         * $this->pushToLog(
         * $alias.': Alias not found. Zip will be downloaded.',
         * 'comment'
         * );
         * $update = true;
         * }
         * $remoteVersion = $this->getRemoteVersion($alias, $projectKey, $mother);
         * if ($version === $remoteVersion) {
         * $this->pushToLog(
         * $alias.': Local version: '.$version.' Remote version: '.$remoteVersion.' - skippping',
         * 'comment'
         * );
         * $update = false;
         * }
         *
         * return $update;
         **/
        $this->pushToLog('Versions not implemented for themes. Proceeding with size check...', 'comment');

        return $this->checkLocalZip($slug, $themeName, $zipPath, $mother, $projectKey, $branchCode);
    }

    /**
     * @param string $alias
     * @param string $projectKey
     * @param string $mother
     * @return string
     * @throws \October\Rain\Exception\ApplicationException
     * @throws \ApplicationException
     */
    private function getRemoteVersion($alias, $projectKey, $mother)
    {
        $deployType = \Config::get('octexchange.spawn::deploy_type');
        $response = $this->requestSender->sendPostRequest(
            [
                'alias'       => $alias,
                'project_key' => $projectKey,
                'deploy_type' => $deployType,
            ],
            $mother.'/api/v1/theme/version'
        );
        if (!array_key_exists(200, $response)) {
            throw new \ApplicationException(
                'Error with connecting to upstream for version check: '.max(array_keys($response))
            );
        }
        $details = json_decode($response[200]['details'], true);

        return $details['version'];

    }

    /**
     * @param string $slug
     * @param string $projectKey
     * @param string $mother
     * @param string $branchCode
     * @return null|string
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function downloadThemeZip($slug, $projectKey, $mother, $branchCode)
    {

        if (!$slug) {
            $this->pushToConsole('Skipping theme...', 'comment');

            return null;
        }
        $deployType = \Config::get('octexchange.spawn::deploy_type');
        $response = $this->requestSender->sendPostRequest(
            [
                'project_key' => $projectKey,
                'slug'        => $slug,
                'deploy_type' => $deployType,
            ],
            $mother.'/api/v1/get-theme'
        );

        return $this->downloadZip($response, $slug, $branchCode);

    }

    /**
     * @param $response
     * @param $slug
     * @param $branchCode
     * @return string
     * @throws \ApplicationException
     */
    public function downloadZip($response, $slug, $branchCode)
    {
        $this->pushToConsole('Downloading theme zip file...', 'comment');
        if (!is_dir(temp_path().'/'.$branchCode)) {
            $this->fileTools->mkdir(temp_path().'/'.$branchCode);
        }
        if (!array_key_exists(200, $response)) {
            throw new \ApplicationException(\Lang::trans('octexchange.spawn::lang.errors.error_downloading'));
        }
        $zipPath = temp_path().'/'.$branchCode.'/'.$slug.'.zip';
        if (is_file($zipPath)) {
            unlink($zipPath);
        }
        $coreResponse = $response[200];
        $stream = $coreResponse['details'];
        $file = fopen($zipPath, 'w+');
        fwrite($file, $stream);
        fclose($file);

        return $zipPath;
    }

    /**
     * This should be replaced by version.yaml comparison
     *
     * @param string $slug
     * @param string $themeName
     * @param string $zipPath
     * @param string $mother
     *
     * @param        $projectKey
     * @param        $branchCode
     * @return bool
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function checkLocalZip($slug, $themeName, $zipPath, $mother, $projectKey, $branchCode)
    {
        $this->pushToConsole('Checking local zip file...', 'info');
        $deployType = \Config::get('octexchange.spawn::deploy_type');

        if (!is_file($zipPath)) {
            $this->downloadThemeZip($slug, $projectKey, $mother, $branchCode);
        }
        $localSize = filesize($zipPath);
        $remoteSize = 0;

        $checkSizeResponse = $this->requestSender->sendPostRequest(
            ['slug' => $slug, 'project_key' => $projectKey, 'deploy_type' => $deployType],
            $mother.'/api/v1/check-theme'
        );
        if (array_key_exists(401, $checkSizeResponse)) {
            throw new ApplicationException('Not authorised');
        }
        if ($checkSizeResponse && array_key_exists(200, $checkSizeResponse)) {
            $details = json_decode($checkSizeResponse[200]['details'], true);
            if (array_key_exists('size', $details)) {
                $remoteSize = $details['size'];
            }
        }

        $update = $remoteSize !== $localSize;
        if ($update) {
            $this->pushToLog(
                $themeName.': [local = '.$localSize.', remote = '.$remoteSize.'] - updating...',
                'info'
            );
        } else {
            $this->pushToLog(
                $themeName.': [local = '.$localSize.', remote = '.$remoteSize.'] - skipping...',
                'comment'
            );
        }

        return $update;
    }

    /**
     * @param string $slug
     * @param string $branchCode
     * @param string $zipPath
     *
     * @internal param array $data
     */
    public function installTheme($slug, $branchCode, $zipPath)
    {

        /**
         * We define branch code (for directory), namespace, plugin name, it's internal October alias
         */
        $this->consoleEmptyLine();
        $this->pushToLog('Installing '.$slug.'...', 'question');
        $branchCode = str_replace('/', '-', $branchCode);
        $themeName = $slug;
        /**
         * ...and two paths - one to extract zip and second to which the proper folder should be moved later.
         */
        $themePath = base_path('themes/'.strtolower($themeName));
        if (is_dir($themePath)) {
            $this->fileTools->rmdir($themePath); // this is installation. we remove anything that may exists.
            $this->pushToConsole($themePath.' removed', 'comment');
        }
        $tempPath = temp_path().'/unzipped/'.$slug;
        /**
         * Then we extract ZIP and move the plugin to its proper directory
         */
        $this->pushToConsole('Extracting to '.$tempPath.'...', 'comment');

        $this->zipTools->extract($zipPath, $tempPath);
        $this->pushToConsole('Extracted.', 'info');
        rename($tempPath.'/'.$branchCode, $themePath);
        $this->pushToConsole('Moved to '.$themePath, 'info');
        $manager = ThemeManager::instance();
        $manager->setInstalled($themeName, strtolower($themeName));
        // TODO: activate?
        $this->pushToConsole('Theme loaded', 'info');
    }

    /**
     * @param        $slug
     * @param        $branchCode
     * @param string $zipPath
     *
     */
    public function proceedWithUpdate($slug, $branchCode, $zipPath)
    {
        $this->consoleEmptyLine();
        $this->pushToLog('Updating '.$slug.'...', 'question');
        /**
         * We define paths
         */
        $themePath = $slug;
        $tempPath = $this->fileTools->getTempPath($slug);
        /**
         * We move current plugin to backup
         */
        $this->pushToConsole('Moving current plugin to backup...', 'comment');
        $this->cleanDirectory($tempPath);
        rename(base_path('themes/'.$themePath), $tempPath);

        $this->pushToConsole('Moved.', 'info');
        $tempZipPath = temp_path().'/unzipped/'.strtolower($themePath);
        $branchCode = str_replace('/', '-', $branchCode);
        /**
         * Then we extract ZIP and move the plugin to its proper directory
         */
        $this->pushToConsole('Extracting to '.$tempZipPath.'...', 'comment');
        $this->cleanDirectory($tempZipPath);
        $this->zipTools->extract($zipPath, $tempZipPath);
        $this->pushToConsole('Extracted.', 'info');
        $this->pushToConsole('Moving to '.$themePath.'...', 'comment');
        rename($tempZipPath.'/'.$branchCode, base_path('themes/'.$slug));
        $this->pushToConsole('Moved to '.$themePath, 'info');
        // TODO: this should be in settings
        $this->fileTools->rmdir($tempPath);
    }

    /**
     * @param $path
     */
    private function cleanDirectory($path)
    {

        if (is_dir($path)) {
            $this->fileTools->rmdir($path);
        }

        $this->fileTools->mkdir($path);
    }

    /**
     * Run composer
     * @throws \Exception
     */
    public function runComposer()
    {
        putenv('COMPOSER_HOME=/tmp');
        $oldLimit = ini_get('memory_limit');
        // this is ugly but there is no way composer filesystem will not reach above 150 mb
        ini_set('memory_limit', '-1');
        $input = new ArrayInput(['command' => 'update']);
        $composer = new ComposerApp();
        $composer->setAutoExit(false); // prevent `$application->run` method from exitting the script
        $composer->run($input);
        $this->pushToLog('Composer update finished', 'info');
        $input = new ArrayInput(['command' => 'dump-autoload']);
        $composer->run($input);
        $this->pushToLog('Composer dump autoload finished', 'info');
        // and we set it to old value as soon as it's finished
        ini_set('memory_limit', $oldLimit);
    }

    /**
     * @param string $string
     * @param        $type
     */
    private function pushToLog($string, $type)
    {
        $this->pushToConsole($string, $type);

        $this->log[] = Carbon::now()->toDateTimeString().': '.$string;
    }

    /**
     * @return int
     */
    private function saveLog()
    {
        $updateLog = new UpdateLog();
        $updateLog->update_details = json_encode($this->log);
        $updateLog->save();

        return $updateLog->id;
    }

    /**
     * @param array $plugin
     *
     * @throws \ValidationException
     */
    private function validatePluginInfo(array $plugin)
    {
        $rules = [
            'name'         => 'required',
            'git_ssh_path' => 'required',
            'git_branch'   => 'required',
            'namespace'    => 'required',
            'directory'    => 'required',
        ];
        $v = \Validator::make($plugin, $rules);
        if ($v->fails()) {
            throw new \ValidationException($v);
        }
    }

    /**
     * @param $response
     *
     * @throws \ValidationException
     */
    private function validateResponse($response)
    {
        $responseRules = [
            'theme'   => 'required|array',
            'project' => 'required',
        ];
        $v = \Validator::make($response, $responseRules);
        if ($v->fails()) {
            throw new \ValidationException($v);
        }
        $pluginRules = [
            'name'         => 'required',
            'git_ssh_path' => 'required',
            'git_branch'   => 'required',
            'namespace'    => 'required',
            'directory'    => 'required',
        ];
        foreach ($response['plugins'] as $plugin) {
            $v = \Validator::make($plugin, $pluginRules);
            if ($v->fails()) {
                throw new \ValidationException($v);
            }
        }
    }

}