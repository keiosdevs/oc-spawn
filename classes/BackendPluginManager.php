<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/20/17
 * Time: 8:54 PM
 */

namespace OctExchange\Spawn\Classes;

use Backend\Classes\WidgetBase;
use OctExchange\Spawn\Models\Settings;
use System\Classes\PluginManager;

/**
 * Class BackendPluginManager
 * @package OctExchange\Spawn\Classes
 */
class BackendPluginManager extends WidgetBase
{
    /**
     *
     */
    const relative_settings_url = 'system/settings/update/octexchange/spawn/settings';

    /**
     * @var string
     */
    protected $defaultAlias = 'backend_plugin_manager';

    /**
     * @var static
     */
    private $manager;

    /**
     * @var string
     */
    private $projectKey;

    /**
     * @var MotherConnector
     */
    private $conn;

    /**
     * @var ProjectUpdater
     */
    private $projectUpdater;

    /**
     * Connect to mother and initialize data
     */
    public function initialize(){
        $this->manager = PluginManager::instance();
        /** @var Settings $settings */
        $settings = Settings::instance();
        $ssl = $settings->get('secure_mother');
        $this->projectKey = $settings->get('project_key');
        $this->conn = new MotherConnector($ssl);
        $this->projectUpdater = new ProjectUpdater($this->conn, $this->projectKey);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function onEnable()
    {
        $this->manager = PluginManager::instance();
        $data = \Input::all();
        $alias = $data['alias'];
        $this->manager->enablePlugin($alias);
        \Flash::success(trans('octexchange.spawn::lang.strings.enabled_successfully'));

        return \Redirect::to(\Backend::url(self::relative_settings_url));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function onDisable()
    {
        $this->manager = PluginManager::instance();
        $data = \Input::all();
        $alias = $data['alias'];
        $this->manager->disablePlugin($alias);
        \Flash::success(trans('octexchange.spawn::lang.strings.disabled_successfully'));

        return \Redirect::to(\Backend::url(self::relative_settings_url));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function onUpdateSingle()
    {
        $this->initialize();
        $data = \Input::all();
        $alias = $data['alias'];
        $this->projectUpdater->updatePlugin($alias);
        \Flash::success(trans('octexchange.spawn::lang.strings.updated_successfully'));

        return \Redirect::to(\Backend::url(self::relative_settings_url));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \OctExchange\Spawn\Exceptions\ExchangeException
     * @throws \ApplicationException
     */
    public function onUpdateAll()
    {
        $this->initialize();
        /** @var Settings $settings */
        $settings = Settings::instance();
        $ssl = $settings->checkForceSsl();
        if (!$this->projectKey) {
            throw new \ApplicationException(\Lang::trans('octexchange.spawn::lang.errors.no_project_key'));
        }
        $this->conn->authorizeProject($this->projectKey);

        $this->projectUpdater->updateAll();
        \Flash::success(trans('octexchange.spawn::lang.strings.updated_successfully'));

        return \Redirect::to(\Backend::url(self::relative_settings_url));

    }
}