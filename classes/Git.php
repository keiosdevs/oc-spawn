<?php

namespace OctExchange\Spawn\Classes;

/*
 * Git.php
 *
 * A PHP git library
 *
 * @package    Git.php
 * @version    0.1.4
 * @author     James Brumond
 * @copyright  Copyright 2013 James Brumond
 * @repo       http://github.com/kbjr/Git.php
 */

if (__FILE__ === $_SERVER['SCRIPT_FILENAME']) {
    die('Bad load order');
}

// ------------------------------------------------------------------------

/**
 * Git Interface Class
 *
 * This class enables the creating, reading, and manipulation
 * of git repositories.
 *
 * @class  Git
 */
class Git
{

    /**
     * Git executable location
     *
     * @var string
     */
    protected static $bin = '/usr/bin/git';

    /**
     * Sets git executable path
     *
     * @param string $path executable location
     */
    public static function setBin($path)
    {
        self::$bin = $path;
    }

    /**
     * Gets git executable path
     */
    public static function getBin()
    {
        return self::$bin;
    }

    /**
     * Sets up library for use in a default Windows environment
     */
    public static function windowsMode()
    {
        self::setBin('git');
    }

    /**
     * Create a new git repository
     *
     * Accepts a creation path, and, optionally, a source path
     *
     * @access  public
     *
     * @param   string $repo_path repository path
     * @param   string $source    directory to source
     *
     * @return  GitRepo
     * @throws \Exception
     */
    public static function &create($repo_path, $source = null)
    {
        return GitRepo::createNew($repo_path, $source);
    }

    /**
     * Open an existing git repository
     *
     * Accepts a repository path
     *
     * @param   string $repo_path repository path
     *
     * @return  GitRepo
     */
    public static function open($repo_path)
    {
        return new GitRepo($repo_path);
    }

    /**
     * Clones a remote repo into a directory and then returns a GitRepo object
     * for the newly created local repo
     *
     * Accepts a creation path and a remote to clone from
     *
     * @access  public
     *
     * @param   string $repo_path repository path
     * @param   string $remote    remote source
     * @param   string $reference reference path
     *
     * @return  GitRepo
     * @throws \Exception
     **/
    public static function &cloneRemote($repo_path, $remote, $reference = null)
    {
        //Changed the below boolean from true to false, since this appears to be a bug when not using a reference repo.  A more robust solution may be appropriate to make it work with AND without a reference.
        return GitRepo::createNew($repo_path, $remote, $remote, $reference);
    }

    /**
     * Checks if a variable is an instance of GitRepo
     *
     * Accepts a variable
     *
     * @access  public
     *
     * @param   mixed $var variable
     *
     * @return  bool
     */
    public static function isRepo($var)
    {
        return (get_class($var) === 'GitRepo');
    }

}