<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 2017-02-27
 * Time: 13:12
 */

namespace OctExchange\Spawn\Classes;


use ZipArchive;

/**
 * Class ZipTools
 *
 * @package OctExchange\Spawn\Classes
 */
class ZipTools
{
    /**
     * @param $zipPath
     * @param $extractPath
     *
     * @return bool
     * @throws \ApplicationException
     */
    public function extract($zipPath, $extractPath)
    {
        $zip = new ZipArchive;
        if ($zip->open($zipPath) === true) {
            $res = $zip->extractTo($extractPath);
            $zip->close();
        } else {
            throw new \ApplicationException(\Lang::trans('octexchange.spawn::lang.errors.cannot_unzip_archive'));
        }

        return $res;
    }
}