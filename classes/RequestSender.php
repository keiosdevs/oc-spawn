<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/16/17
 * Time: 1:23 PM
 */

namespace OctExchange\Spawn\Classes;

use October\Rain\Exception\ApplicationException;

/**
 * Class RequestSender
 *
 * @package OctExchange\Spawn\Classes
 */
class RequestSender
{
    /**
     * Headers array
     *
     * @var array
     */
    protected $headers = [];

    private $forceSsl = false;

    /**
     * RequestSender constructor.
     *
     * @param string      $contentType
     * @param string|null $bearerToken
     */
    public function __construct($bearerToken = null, $contentType = 'application/json')
    {
        $this->headers[] = 'Content-Type: '.$contentType;
        if ($bearerToken) {
            $this->headers[] = 'Authorization: Bearer '.$bearerToken;
        }
    }

    /**
     * Send post request to given url
     *
     * @param array  $data
     * @param string $url
     *
     * @return array Http Code => ['detail']
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function sendPostRequest(array $data, $url)
    {
        $oldLimit = ini_get('memory_limit');
        // this is ugly but there is no way composer filesystem will not reach above 150 mb
        ini_set('memory_limit', '-1');

        $response = [];
        if($this->forceSsl){
            if (strpos($url, 'http://') !== false) {
                $url = str_replace('http://', 'https://', $url);
            } else {
                $url = 'https://'.$url;
            }
        }
        $requestData = json_encode($data, JSON_UNESCAPED_SLASHES);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        if (false === $result) {
            $this->handleError($ch, $data, $url);
        }

        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response[$responseCode] = [
            'code'    => $responseCode,
            'details' => $result,
        ];

        curl_close($ch);
        ini_set('memory_limit', $oldLimit);

//        \Log::debug('Request: '.print_r($data, true).' to '.$url.PHP_EOL.'Post Response: '.print_r($result, true));

        return $response;
    }

    /**
     * Send put request to given url
     *
     * @param array  $data
     * @param string $url
     *
     * @return array Http Code
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function sendPutRequest(array $data, $url)
    {
        $response = [];
        $requestData = http_build_query($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        if (false === $result) {
            $this->handleError($ch, $data, $url);
        }
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response[$responseCode] = [
            'code'    => $responseCode,
            'details' => $result,
        ];

        curl_close($ch);
        \Log::debug('Put Response: '.print_r($result, true));

        return $response;
    }

    /**
     * Send put request to given url
     *
     * @param array  $data
     * @param string $url
     *
     * @return array Http Code
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function sendGetRequest(array $data, $url)
    {
        $response = [];
        $requestData = http_build_query($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        if (false === $result) {
            $this->handleError($ch, $data, $url);
        }
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response[$responseCode] = [
            'code'    => $responseCode,
            'details' => $result,
        ];

        curl_close($ch);
        \Log::debug('Get Response: '.print_r($result, true));

        return $response;
    }

    /**
     * Send delete request to given url
     *
     * @param array  $data
     * @param string $url
     *
     * @return array
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function sendDeleteRequest(array $data, $url)
    {
        $response = [];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        if (false === $result) {
            $this->handleError($ch, $data, $url);
        }
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response[$responseCode] = [
            'code'    => $responseCode,
            'details' => $result,
        ];

        curl_close($ch);
        \Log::debug('Delete Response: '.print_r($result, true));

        return $response;
    }

    /**
     * Add custom header
     *
     * @param string $header
     */
    public function addHeader($header)
    {
        $this->headers[] = $header;
    }

    /**
     * Log error and throw exception
     *
     * @param resource $ch
     * @param array    $data
     * @param string   $url
     *
     * @throws ApplicationException
     */
    protected function handleError($ch, $data, $url)
    {
        \Log::error(
            'Request error for url: '.$url.' and data: '
            .print_r($data, true)
            .': '.curl_error($ch)
            .' code:'.curl_error(
                $ch
            )
        );

        throw new ApplicationException(curl_error($ch), curl_errno($ch));
    }

    /**
     * @param bool $forceSsl
     */
    public function setForceSsl($forceSsl)
    {
        $this->forceSsl = $forceSsl;
    }
}