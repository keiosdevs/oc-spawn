<?php

namespace OctExchange\Spawn\Classes;

use System\ServiceProvider;

/**
 * Class ConsoleOutputServiceProvider
 * @package OctExchange\Spawn\Classes
 */
class ConsoleOutputServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        \App::bind(
            'consoleOutput',
            function () {
                return new \Symfony\Component\Console\Output\ConsoleOutput();
            }
        );
    }
}