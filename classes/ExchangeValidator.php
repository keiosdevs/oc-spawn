<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/23/17
 * Time: 4:16 PM
 */

namespace OctExchange\Spawn\Classes;

use ApplicationException;
use Validator;
use ValidationException;

/**
 * Class ExchangeValidator
 * @package OctExchange\Spawn\Classes
 */
class ExchangeValidator
{
    /**
     * @param array $plugin
     *
     * @throws ValidationException
     */
    public static function validatePluginInfo(array $plugin)
    {
        $rules = [
            'name'         => 'required',
            'git_ssh_path' => 'required',
            'git_branch'   => 'required',
            'namespace'    => 'required',
            'directory'    => 'required',
        ];
        $v = Validator::make($plugin, $rules);
        if ($v->fails()) {
            throw new ValidationException($v);
        }
    }

    /**
     * @param array $response
     *
     * @throws ValidationException
     */
    public static function validateResponse(array $response)
    {
        $responseRules = [
            'plugins' => 'array',
            'project' => 'required',
        ];
        $v = Validator::make($response, $responseRules);
        if ($v->fails()) {
            throw new ValidationException($v);
        }
        $pluginRules = [
            'name'         => 'required',
            'git_ssh_path' => 'required',
            'git_branch'   => 'required',
            'namespace'    => 'required',
            'directory'    => 'required',
        ];
        foreach ($response['plugins'] as $plugin) {
            $v = Validator::make($plugin, $pluginRules);
            if ($v->fails()) {
                throw new ValidationException($v);
            }
        }
    }

    /**
     * TODO: replace not looping loop with something else
     *
     * @param array $response
     * @throws ApplicationException
     */
    public static function prevalidateResponse(array $response)
    {
        if (!array_key_exists(200, $response)) {
            // TODO
            foreach ($response as $code => $details) {
                if ($code === 401) {
                    throw new ApplicationException('Invalid project key');
                }

                throw new ApplicationException(
                    \Lang::trans(
                        'octexchange.spawn::lang.errors.unknown_error_while_connecting_to_upstream'
                    ).': '.$code
                );
            }
        }
    }

    /**
     * @param array $data
     *
     * @throws \ValidationException
     */
    public static function validateAuthentication(array $data)
    {
        $rules = [
            'response' => 'required',
            'mother'   => 'required',
        ];

        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            throw new ValidationException($v);
        }
    }

    /**
     * @param array $plugins
     * @throws \ValidationException
     */
    public static function validatePlugins(array $plugins)
    {
        foreach ($plugins as $plugin) {
            self::validatePluginInfo($plugin);
        }
    }
}