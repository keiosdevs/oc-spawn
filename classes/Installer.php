<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/15/17
 * Time: 8:54 AM
 */

namespace OctExchange\Spawn\Classes;

use OctExchange\Spawn\Facades\ConsoleOutput;

class Installer
{
    /**
     * @param $string
     * @param $type
     */
    protected function pushToConsole($string, $type)
    {
        if (PHP_SAPI === 'cli') {
            ConsoleOutput::writeln("<$type>$string</$type>");
        }
    }
    /**
     *
     */
    protected function consoleEmptyLine()
    {
        $this->pushToConsole('', 'info');
    }

}