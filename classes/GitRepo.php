<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 5/21/17
 * Time: 1:18 PM
 */

namespace OctExchange\Spawn\Classes;

use ApplicationException;

/**
 * Class GitRepo
 * @package OctExchange\Spawn\Classes
 */
class GitRepo
{

    /**
     * @var string|null
     */
    protected $repoPath;
    /**
     * @var bool
     */
    protected $bare = false;
    /**
     * @var array
     */
    protected $envopts = [];

    /**
     * @var FileTools
     */
    private $fileTools;

    /**
     * Constructor
     *
     * Accepts a repository path
     *
     * @access  public
     *
     * @param   string $repoPath  repository path
     * @param   bool   $createNew create if not exists?
     * @param   bool   $init      initialize new
     * @throws  ApplicationException
     */
    public function __construct(
        $repoPath = null,
        $createNew = false,
        $init = true
    ) {
        if (is_string($repoPath)) {
            $this->setRepoPath($repoPath, $createNew, $init);
        }
        $this->fileTools = new FileTools();
    }

    /**
     * Create a new git repository
     *
     * Accepts a creation path, and, optionally, a source path
     *
     * @access   public
     *
     * @param string      $repo_path
     * @param null|string $source
     * @param bool        $remote_source
     * @param null|string $reference
     *
     * @return GitRepo
     * @throws ApplicationException
     *
     */
    public static function &createNew($repo_path, $source = null, $remote_source = false, $reference = null)
    {
        if (is_dir($repo_path) && file_exists($repo_path.'/.git')) {
            throw new ApplicationException('"'.$repo_path.'" is already a git repository');
        }
        $repo = new GitRepo($repo_path, true, false);
        if (is_string($source)) {
            if ($remote_source) {
                if (isset($reference)) {
                    if (!is_dir($reference) || !is_dir($reference.'/.git')) {
                        throw new ApplicationException(
                            '"'.$reference.'" is not a git repository. Cannot use as reference.'
                        );
                    }
                    if (strlen($reference)) {
                        $reference = realpath($reference);
                        $reference = "--reference $reference";
                    }

                }
                $repo->cloneRemote($source, $reference);
            } else {
                $repo->cloneFrom($source);
            }
        } else {
            $repo->run('init');
        }

        return $repo;

    }

    /**
     * Set the repository's path
     *
     * Accepts the repository path
     *
     * TODO: god have mercy
     *
     * @access  public
     *
     * @param   string $repoPath   repository path
     * @param   bool   $create_new create if not exists?
     * @param   bool   $init       initialize new Git repo if not exists?
     *
     * @throws ApplicationException
     */
    public function setRepoPath($repoPath, $create_new = false, $init = true)
    {
        if (is_string($repoPath)) {
            if ($newPath = realpath($repoPath)) {
                $repoPath = $newPath;
                if (is_dir($repoPath)) {
                    // Is this a work tree?
                    if (file_exists($repoPath.'/.git')) {
                        $this->repoPath = $repoPath;
                        $this->bare = false;
                        // Is this a bare repo?
                    } else {
                        if (is_file($repoPath.'/config')) {
                            $parse_ini = parse_ini_file($repoPath.'/config');
                            if ($parse_ini['bare']) {
                                $this->repoPath = $repoPath;
                                $this->bare = true;
                            }
                        } else {
                            if ($create_new) {
                                $this->repoPath = $repoPath;
                                if ($init) {
                                    $this->run('init');
                                }
                            } else {
                                throw new ApplicationException('"'.$repoPath.'" is not a git repository');
                            }
                        }
                    }
                } else {
                    throw new ApplicationException('"'.$repoPath.'" is not a directory');
                }
            } else {
                if ($create_new) {
                    if ($parent = realpath(dirname($repoPath))) {
                        $this->fileTools->mkdir($repoPath);
                        $this->repoPath = $repoPath;
                        if ($init) {
                            $this->run('init');
                        }
                    } else {
                        throw new ApplicationException('cannot create repository in non-existent directory');
                    }
                } else {
                    throw new ApplicationException('"'.$repoPath.'" does not exist');
                }
            }
        }
    }

    /**
     * Get the path to the git repo directory (eg. the ".git" directory)
     *
     * @access public
     * @return string
     * @throws ApplicationException
     */
    public function gitDirectoryPath()
    {
        if ($this->bare) {
            return $this->repoPath;
        }
        if (is_dir($this->repoPath.'/.git')) {
            return $this->repoPath.'/.git';
        }
        if (is_file($this->repoPath.'/.git')) {
            $gitFile = file_get_contents($this->repoPath.'/.git');
            if (mb_ereg('^gitdir: (.+)$', $gitFile, $matches = []) && $matches[1]) {
                $relGitPath = $matches[1];

                return $this->repoPath.'/'.$relGitPath;

            }
        }

        throw new ApplicationException('could not find git dir for '.$this->repoPath.'.');
    }

    /**
     * Tests if git is installed
     *
     * @access  public
     * @return  bool
     */
    public function testGit()
    {
        $descriptorspec = [
            1 => ['pipe', 'w'],
            2 => ['pipe', 'w'],
        ];
        $pipes = [];
        $resource = proc_open(Git::getBin(), $descriptorspec, $pipes);

//        $stdout = stream_get_contents($pipes[1]);
//        $stderr = stream_get_contents($pipes[2]);
        foreach ($pipes as $pipe) {
            fclose($pipe);
        }

        $status = trim(proc_close($resource));

        return ($status !== 127);
    }

    /**
     * Run a command in the git repository
     *
     * Accepts a shell command to run
     *
     * @access  protected
     *
     * @param   string $command command to run
     *
     * @return string
     * @throws ApplicationException
     */
    protected function runCommand(
        $command
    ) {
        $descriptorspec = [
            1 => ['pipe', 'w'],
            2 => ['pipe', 'w'],
        ];
        $pipes = [];
        /* Depending on the value of variables_order, $_ENV may be empty.
         * In that case, we have to explicitly set the new variables with
         * putenv, and call proc_open with env=null to inherit the reset
         * of the system.
         *
         * This is kind of crappy because we cannot easily restore just those
         * variables afterwards.
         *
         * If $_ENV is not empty, then we can just copy it and be done with it.
         */
        if (count($_ENV) === 0) {
            $env = null;
            foreach ($this->envopts as $k => $v) {
                putenv(sprintf('%s=%s', $k, $v));
            }
        } else {
            $env = array_merge($_ENV, $this->envopts);
        }
        $cwd = $this->repoPath;

        $resource = proc_open($command, $descriptorspec, $pipes, $cwd, $env);

        $stdout = stream_get_contents($pipes[1]);
        $stderr = stream_get_contents($pipes[2]);
        foreach ($pipes as $pipe) {
            fclose($pipe);
        }

        $status = trim(proc_close($resource));
        if ($status) {
            throw new ApplicationException($stderr."\n".$stdout);
        } //Not all errors are printed to stderr, so include std out as well.

        return $stdout;
    }

    /**
     * Run a git command in the git repository
     *
     * Accepts a git command to run
     *
     * @access  public
     *
     * @param   string $command command to run
     *
     * @return  string
     * @throws  ApplicationException
     */
    public function run(
        $command
    ) {
        return $this->runCommand(Git::getBin().' '.$command);
    }

    /**
     * Runs a 'git status' call
     *
     * Accept a convert to HTML bool
     *
     * @access public
     *
     * @param bool $html return string with <br />
     *
     * @return string
     * @throws ApplicationException
     */
    public function status(
        $html = false
    ) {
        $msg = $this->run('status');
        if ($html === true) {
            $msg = str_replace("\n", '<br />', $msg);
        }

        return $msg;
    }

    /**
     * Runs a `git add` call
     *
     * Accepts a list of files to add
     *
     * @access  public
     *
     * @param   mixed $files files to add
     *
     * @return  string
     * @throws ApplicationException
     */
    public function add(
        $files = '*'
    ) {
        if (is_array($files)) {
            $files = '"'.implode('" "', $files).'"';
        }

        return $this->run("add $files -v");
    }

    /**
     * Runs a `git rm` call
     *
     * Accepts a list of files to remove
     *
     * @access  public
     *
     * @param   mixed   $files  files to remove
     * @param   Boolean $cached use the --cached flag?
     *
     * @return  string
     * @throws  ApplicationException
     */
    public function rm(
        $files = '*',
        $cached = false
    ) {
        if (is_array($files)) {
            $files = '"'.implode('" "', $files).'"';
        }

        return $this->run('rm '.($cached ? '--cached ' : '').$files);
    }

    /**
     * Runs a `git commit` call
     *
     * Accepts a commit message string
     *
     * @access  public
     *
     * @param   string  $message    commit message
     * @param   boolean $commit_all should all files be committed automatically (-a flag)
     *
     * @return  string
     * @throws  ApplicationException
     */
    public function commit(
        $message = '',
        $commit_all = true
    ) {
        $flags = $commit_all ? '-av' : '-v';

        return $this->run('commit '.$flags.' -m '.escapeshellarg($message));
    }

    /**
     * Runs a `git clone` call to clone the current repository
     * into a different directory
     *
     * Accepts a target directory
     *
     * @access  public
     *
     * @param   string $target target directory
     *
     * @return  string
     * @throws  ApplicationException
     */
    public function cloneTo(
        $target
    ) {
        return $this->run('clone --local '.$this->repoPath." $target");
    }

    /**
     * Runs a `git clone` call to clone a different repository
     * into the current repository
     *
     * Accepts a source directory
     *
     * @access  public
     *
     * @param   string $source source directory
     *
     * @return  string
     * @throws  ApplicationException
     */
    public function cloneFrom(
        $source
    ) {
        return $this->run("clone --local $source ".$this->repoPath);
    }

    /**
     * Runs a `git clone` call to clone a remote repository
     * into the current repository
     *
     * Accepts a source url
     *
     * @access  public
     *
     * @param   string $source    source url
     * @param   string $reference reference path
     *
     * @return  string
     * @throws  ApplicationException
     */
    public function cloneRemote(
        $source,
        $reference
    ) {
        return $this->run("clone $reference $source ".$this->repoPath);
    }

    /**
     * Runs a `git clean` call
     *
     * Accepts a remove directories flag
     *
     * @access  public
     *
     * @param   bool $dirs  delete directories?
     * @param   bool $force force clean?
     *
     * @return  string
     * @throws  ApplicationException
     */
    public function clean(
        $dirs = false,
        $force = false
    ) {
        return $this->run('clean'.($force ? ' -f' : '').($dirs ? ' -d' : ''));
    }

    /**
     * Runs a `git branch` call
     *
     * Accepts a name for the branch
     *
     * @access  public
     *
     * @param   string $branch branch name
     *
     * @return  string
     * @throws  ApplicationException
     */
    public function createBranch(
        $branch
    ) {
        return $this->run('branch '.escapeshellarg($branch));
    }

    /**
     * Runs a `git branch -[d|D]` call
     *
     * Accepts a name for the branch
     *
     * @access  public
     *
     * @param string $branch branch name
     * @param bool   $force  force?
     *
     * @return  string
     * @throws \ApplicationException
     */
    public function deleteBranch(
        $branch,
        $force = false
    ) {
        return $this->run('branch '.($force ? '-D' : '-d')." $branch");
    }

    /**
     * Runs a `git branch` call
     *
     * @access  public
     *
     * @param   bool $keep_asterisk keep asterisk mark on active branch
     *
     * @return  array
     * @throws \ApplicationException
     */
    public function listBranches(
        $keep_asterisk = false
    ) {
        $branchArray = explode("\n", $this->run('branch'));
        foreach ($branchArray as $i => &$branch) {
            $branch = trim($branch);
            if (!$keep_asterisk) {
                $branch = str_replace('* ', '', $branch);
            }
            if (!$branch) {
                unset($branchArray[$i]);
            }
        }

        return $branchArray;
    }

    /**
     * Lists remote branches (using `git branch -r`).
     *
     * Also strips out the HEAD reference (e.g. "origin/HEAD -> origin/master").
     *
     * @access  public
     * @return  array
     * @throws \ApplicationException
     */
    public function listRemoteBranches()
    {
        $branchArray = explode("\n", $this->run('branch -r'));
        foreach ($branchArray as $i => &$branch) {
            $branch = trim($branch);
            if (!$branch || strpos($branch, 'HEAD -> ') !== false) {
                unset($branchArray[$i]);
            }
        }

        return $branchArray;
    }

    /**
     * Returns name of active branch
     *
     * @access  public
     *
     * @param   bool $keep_asterisk keep asterisk mark on branch name
     *
     * @return  string
     * @throws \ApplicationException
     */
    public function activeBranch($keep_asterisk = false)
    {
        $branchArray = $this->listBranches(true);
        $active_branch = preg_grep("/^\*/", $branchArray);
        reset($active_branch);
        if ($keep_asterisk) {
            return current($active_branch);
        }

        return str_replace('* ', '', current($active_branch));

    }

    /**
     * Runs a `git checkout` call
     *
     * Accepts a name for the branch
     *
     * @access  public
     *
     * @param   string $branch branch name
     *
     * @return  string
     * @throws \ApplicationException
     */
    public function checkout($branch)
    {
        return $this->run('checkout '.escapeshellarg($branch));
    }

    /**
     * Runs a `git merge` call
     *
     * Accepts a name for the branch to be merged
     *
     * @access  public
     *
     * @param   string $branch
     *
     * @return  string
     * @throws \ApplicationException
     */
    public function merge($branch)
    {
        return $this->run('merge '.escapeshellarg($branch).' --no-ff');
    }

    /**
     * Runs a git fetch on the current branch
     *
     * @access  public
     * @return  string
     * @throws \ApplicationException
     */
    public function fetch()
    {
        return $this->run('fetch');
    }

    /**
     * Add a new tag on the current position
     *
     * Accepts the name for the tag and the message
     *
     * @param string $tag
     * @param string $message
     *
     * @return string
     * @throws \ApplicationException
     */
    public function add_tag($tag, $message = null)
    {
        if (null === $message) {
            $message = $tag;
        }

        return $this->run("tag -a $tag -m ".escapeshellarg($message));
    }

    /**
     * List all the available repository tags.
     *
     * Optionally, accept a shell wildcard pattern and return only tags matching it.
     *
     * @access    public
     *
     * @param    string $pattern Shell wildcard pattern to match tags against.
     *
     * @return    array                Available repository tags.
     * @throws \ApplicationException
     */
    public function list_tags($pattern = null)
    {
        $tagArray = explode("\n", $this->run("tag -l $pattern"));
        foreach ($tagArray as $i => &$tag) {
            $tag = trim($tag);
            if (empty($tag)) {
                unset($tagArray[$i]);
            }
        }

        return $tagArray;
    }

    /**
     * Push specific branch (or all branches) to a remote
     *
     * Accepts the name of the remote and local branch.
     * If omitted, the command will be "git push", and therefore will take
     * on the behavior of your "push.defualt" configuration setting.
     *
     * @param string $remote
     * @param string $branch
     *
     * @return string
     * @throws \ApplicationException
     */
    public function push($remote = '', $branch = '')
    {
        //--tags removed since this was preventing branches from being pushed (only tags were)
        return $this->run("push $remote $branch");
    }

    /**
     * Pull specific branch from remote
     *
     * Accepts the name of the remote and local branch.
     * If omitted, the command will be "git pull", and therefore will take on the
     * behavior as-configured in your clone / environment.
     *
     * @param string $remote
     * @param string $branch
     *
     * @return string
     * @throws \ApplicationException
     */
    public function pull(
        $remote = '',
        $branch = ''
    ) {
        return $this->run("pull $remote $branch");
    }

    /**
     * List log entries.
     *
     * @param string $format
     * @param bool   $fulldiff
     * @param string $filepath
     * @param bool   $follow
     *
     * @return string
     * @throws \ApplicationException
     */
    public function log(
        $format = null,
        $fulldiff = false,
        $filepath = null,
        $follow = false
    ) {
        $diff = '';
        if ($fulldiff) {
            $diff = '--full-diff -p ';
        }

        if ($follow) {
            // Can't use full-diff with follow
            $diff = '--follow -- ';
        }

        if ($format === null) {
            return $this->run('log '.$diff.$filepath);
        }

        return $this->run('log --pretty=format:"'.$format.'" '.$diff.$filepath);

    }

    /**
     * Sets the project description.
     *
     * @param string $new
     * @throws \Exception
     */
    public function set_description($new)
    {
        $path = $this->gitDirectoryPath();
        file_put_contents($path.'/description', $new);
    }

    /**
     * Gets the project description.
     *
     * @return string
     * @throws \Exception
     */
    public function get_description()
    {
        $path = $this->gitDirectoryPath();

        return file_get_contents($path.'/description');
    }

    /**
     * Sets custom environment options for calling Git
     *
     * @param string $key   key
     * @param string $value value
     */
    public function setenv($key, $value)
    {
        $this->envopts[$key] = $value;
    }
}