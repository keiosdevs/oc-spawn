<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/17/17
 * Time: 1:33 AM
 */

namespace OctExchange\Spawn\Controllers;

use Illuminate\Routing\Controller;
use Input;
use OctExchange\Spawn\Classes\MotherConnector;
use OctExchange\Spawn\Classes\ProjectUpdater;
use OctExchange\Spawn\Models\Settings;
use System\Classes\PluginManager;
use System\Models\PluginVersion;
use ApplicationException;
use ValidationException;

/**
 * Class ApiController
 *
 * Responsible for online list management
 *
 * @package Pixelpixel\Motogamers\Controllers
 */
class ApiController extends Controller
{
    /**
     * @var MotherConnector
     */
    private $conn;

    /**
     * @var ProjectUpdater
     */
    private $installer;

    /**
     * @var string
     */
    private $projectKey;

    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        /** @var Settings $settings */
        $settings = Settings::instance();
        $ssl = $settings->get('secure_mother');
        $this->projectKey = $settings->get('project_key');
        $this->conn = new MotherConnector($ssl);
        $this->installer = new ProjectUpdater($this->conn, $this->projectKey);
    }


     /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function disablePlugin()
    {
        $data = post();
        $sentKey = $data['project_key'];
        if ($sentKey !== $this->projectKey) {
            return response(['message' => 'Not authorized'], 401);
        }
        if (!$this->projectKey) {
            return response(['message' => 'Spawn Not Configured'], 500);
        }
        try {
            $code = $data['plugin'];
            $manager = PluginManager::instance();
            $object = PluginVersion::where('code', $code)->first();
            $manager->disablePlugin($object->code, true);

            return response(['message' => 'OK', 'error' => false], 200);
        } catch (\Exception $e) {
            return response(['message' => 'Invalid request', 'error' => true], 400);
        }
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function enablePlugin()
    {
        $data = post();
        $sentKey = $data['project_key'];

        if ($sentKey !== $this->projectKey) {
            return response(['message' => 'Not authorized'], 401);
        }
        if (!$this->projectKey) {
            return response(['message' => 'Spawn Not Configured'], 500);
        }
        try {
            $code = $data['plugin'];
            $manager = PluginManager::instance();
            $object = PluginVersion::where('code', $code)->first();
            $manager->enablePlugin($object->code, true);

            return response(['message' => 'OK', 'error' => false], 200);
        } catch (\Exception $e) {
            return response(['message' => 'Invalid request', 'error' => true], 400);
        }
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \OctExchange\Spawn\Exceptions\ExchangeException
     * @throws \ApplicationException
     * @throws ValidationException
     * @throws ApplicationException
     */
    public function update()
    {
        $data = Input::all();
        if (!array_key_exists('project_key', $data)) {
            return response(['message' => 'Not authorized'], 401);
        }
        $sentKey = $data['project_key'];
        if ($sentKey !== $this->projectKey) {
            return response(['message' => 'Not authorized'], 401);
        }
        if (!$this->projectKey) {
            return response(['message' => 'Spawn Not Configured'], 500);
        }

        $this->conn->authorizeProject($this->projectKey);

        $this->installer->updateAll();

        return response(['message' => 'OK'], 200);
    }
}