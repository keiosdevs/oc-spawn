<?php namespace OctExchange\Spawn\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Backend Market Back-end Controller
 */
class BackendMarket extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('OctExchange.Spawn', 'spawn', 'backendmarket');
    }
}
