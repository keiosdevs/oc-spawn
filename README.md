# Spawn

This is a client plugin for [OctoberExchange Mother](https://bitbucket.org/keiosdevs/oc-mother).

It should be installed on a client's machine on deploy (exemplary deploy procedure in sh file in this repository)

### New Namespace

Per many requests, namespace for this plugin has changed at version 1.5

Plugin should be stored in plugin/octexchange/spawn now

### Configuration

After plugin installation:

- Go to Backend Settings -> Exchange and enter Project Key from your upstream Mother instance
- Or use php artisan exchange:register <project_key>

That's currently it.


### Commands

```
php artisan exchange:update
```

Used to sync local plugin state with upstream, download and install updates.

Works the same way as *Update* button in Backend Settings.

Will automatically switch your page into Maintaince mode until updates are finished.


### Todo

A lot. Write tests, clean up code, BackendMarket controller etc. 